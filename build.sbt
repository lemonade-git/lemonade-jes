name := """lemonade-jes"""
organization := "be.lemonade"
version := "2.1.4"

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
  "javax.json" % "javax.json-api" % "1.1.4",
  "com.google.inject" % "guice" % "4.2.2",
  "com.typesafe.akka" %% "akka-actor" % "2.5.22",
  "com.typesafe.akka" %% "akka-persistence" % "2.5.22",
  "com.typesafe.akka" %% "akka-cluster-sharding" % "2.5.22",
  "com.typesafe.play" %% "play" % "2.7.2",
  "com.typesafe.play" %% "play-java" % "2.7.2",
  "org.apache.kafka" % "kafka-clients" % "2.2.0",
  "org.mongodb" % "bson" % "3.10.2",
  "org.mongodb" % "mongodb-driver-reactivestreams" % "1.11.0",
  "io.jsonwebtoken" % "jjwt-api" % "0.10.5",
  "net.pincette" % "pincette-common" % "1.5.5" withSources(),
  "io.netty" % "netty-handler" % "4.1.35.Final",
  "software.amazon.awssdk" % "s3" % "2.5.51",
  "software.amazon.awssdk" % "sns" % "2.5.51",
  "be.lemonade" % "lemonade-aws" % "2.1.4" withSources(),
  "be.lemonade" % "lemonade-jes-util" % "2.1.3" withSources()
)

val nexus = "https://nexus.lemonade.be/nexus/content/repositories/"

publishTo := {
  if (isSnapshot.value)
    Some("Sonatype Nexus Repository Manager" at nexus + "snapshots")
  else
    Some("Sonatype Nexus Repository Manager" at nexus + "releases")
}

credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")

resolvers ++= Seq("Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/",
  "Sonatype Nexus Repository Manager Snapshots" at nexus + "snapshots",
  "Sonatype Nexus Repository Manager Releases" at nexus + "releases"
)

crossPaths := false
