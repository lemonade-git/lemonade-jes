# Event Sourcing for JSON

This Java library provides support for event sourcing for the [JSON format](https://www.json.org) using [Play](https://www.playframework.com), [Akka](https://akka.io), [Kafka](https://kafka.apache.org) and [AWS SNS](https://aws.amazon.com/sns). It contains a generic Play controller to manipulate aggregates through a REST API. [Persistent actors](https://doc.akka.io/docs/akka/2.5/persistence.html), which live in a [sharded Akka cluster](https://doc.akka.io/docs/akka/2.5.12/cluster-sharding.html), take care of the commands that are dispatched to them.

The developer provides validators and reducers. Validators inspect incoming commands and annotate them with errors if there are any. Reducers use commands to produce a new version of an aggregate. The persistent actor orchestrates this. When the validator reports errors the annotated command is sent back. If the reducer changes the aggregate the difference with the previous version is saved an event.

Kafka is used to publish events and dispatch commands. SNS is used to send replies back to the user.

## Aggregates

This notion from [Domain Driven Design](https://en.wikipedia.org/wiki/Domain-driven_design) defines a bounded context for some piece of data. You can only access the data through the aggregate root. There is no back door to fiddle with the data directly, such as doing joins, etc.

The consequence is that an aggregate could live anywhere. It only needs its private database. This is the foundation for distributing data over a cluster in a way that scales linearly.

Aggregates usually contain everything that is needed to represent some business entity. Its parts have no life cycle of their own. When aggregates work together they can refer to each other using their mutual aggregate roots.

Because JSON is used everywhere we also maintain and manipulate the aggregates in that format. Of course, validators and reducers are free to marshall and unmarshall it to something else.

For clients working with aggregates is quite easy, because they always receive a business entity as a whole. There is no need to assemble it from different pieces.

## Commands

Commands are JSON documents that are posted to an aggregate. They can have an effect on the aggregate. The application-provided reducer defines the effect a command can have on an aggregate. A reducer receives the current state of the aggregate and the command. It is supposed to generate the new state of the aggregate.

Before being passed to the reducer commands are run through an application-provided validator. This one also receives the current state of the aggregate and the command. When everything is fine it should return the command exactly the way it is. Otherwise it should annotate it with the errors it found. In that case the command won't go to the reducer.

It is the application that defines the data the commands can carry. A command could, for example, be associated with an edit box for part of an aggregate. It would then just carry the corresponding data. The reducer is responsible for putting it correctly in the aggregate.

## Events

These are JSON documents that describe the difference between two subsequent versions of an aggregate. They are save in the event log and published on a Kafka topic with the same name as the type of the aggregate.

## Message Flow

The flow of a message is shown in the figure below. When a request comes in it is immediately stored in a Kafka command topic. The client will receive only an acknowledgement. The message is then dispatched to a persistent actor in the Akka cluster, based on its identifier. The actor is generic. It calls the validator and inspects its results. If there are errors the annotated command is placed on a configured SNS topic. From there it is routed to the user. The flow is then ended.

If there are no errors the actor calls the reducer with the command and the current state of the aggregate. The reducer returns the new state. This is placed on the configured SNS topic. Then the actor compares the new state with the current one. If there are differences a JSON patch, in
[RFC 6902](https://tools.ietf.org/html/rfc6902) format, is saved in the event log. It is also placed on a Kafka topic with the same name as the type of the aggregate.

The generic [Play](https://www.playframework.com) controller has a [server sent events](https://html.spec.whatwg.org/multipage/server-sent-events.html#server-sent-events) endpoint. This sets up a channel that allows all components to send messages to the user via a configured SNS topic.

For anonymous requests there is no username and hence no reply channel. In that case replies go back directly to the response body.

![Message Flow](flow.svg)

## Conventions

We need a few technical JSON fields to manage aggregates and commands. The following tables list them.

### Aggregates

|Name|Description|
|--|--|
|\_id|A globally unique identifier, formatted as a string.|
|\_type|The type name. If several applications share the same cluster a prefix convention may be used to avoid name clashes.|
|\_corr|A correlation identifier, formatted as a string. Clients may use this to associate requests with responses, which come through on the server sent event stream of the websocket.|
|\_seq|The sequence number of the last event. The very first event of an aggregate will have the value zero. Every subsequent event will increment it by one. A "hole" in the sequence indicates corruption.|
|\_deleted|This boolean says whether the aggregate has been deleted or not. A deleted aggregate is not destroyed, but a request for it will yield "Not found".|

### Commands

|Name|Description|
|--|--|
|\_id|A globally unique identifier, formatted as a string.|
|\_type|The type name. If several applications share the same cluster a prefix convention may be used to avoid name clashes.|
|\_corr|A correlation identifier, formatted as a string. Clients may use this to associate requests with responses, which come through on the server sent event stream of the websocket.|
|\_command|The name of the command. It is chosen by the application.|
|\_jwt|This is a [JSON Web Token](https://jwt.io). If it is present it is verified and replaced by the payload, which will be saved along with the event if there is an effect.|
|\_languages|A JSON array of language tags ordered by preference. Components can use this for localised messages to users.|
|\_error|In a response this can be set to `true` to indicate a validation error.|
|\_statusCode|In a response this can be set to a general integer code when the validation of a command fails for the command as a whole, without further details. There is a built-in validation which tests for the existence of the aggregate mentioned in the command. When it doesn't the field will be set to 404.|
|\_test|When this boolean field is set to `true` all replies go back via the response body.| 

There are some built-in command names:

|Name|Description|
|--|--|
|delete|This is the name to which the command will be set when the HTTP method DELETE is received.|
|get|This is the name to which the command will be set when the HTTP method GET is received. In this case the reply will go back directly to the response body.|
|passivate|This will send a poison pill to the corresponding persistent actor. At the next request it will be resurrected in memory.|
|patch|This is the name to which the command will be set when the HTTP method PATCH is received.|
|put|This is the name to which the command will be set when the HTTP method PUT is received.|

### Events

|Name|Description|
|--|--|
|\_id|A globally unique identifier, formatted as a string.|
|\_type|The type name. If several applications share the same cluster a prefix convention may be used to avoid name clashes.|
|\_corr|A correlation identifier, formatted as a string. Clients may use this to associate requests with responses, which come through on the server sent event stream of the websocket.|
|\_seq|The sequence number of the event.|
|\_jwt|This is a [JSON Web Token](https://jwt.io). If it is present it is verified and replaced by the payload, which will be saved along with the event.|
|\_languages|A JSON array of language tags ordered by preference. Components can use this for localised messages to users.|
|\_before|The state of the aggregate before the event was applied. This is not stored in the event log.|
|\_after|The state of the aggregate after the event was applied. This is not stored in the event log.|
|\_ops|The array of operations as described in [RFC 6902](https://tools.ietf.org/html/rfc6902).|

## Security

The REST API expects [JSON Web Tokens](https://jwt.io) to identify users and anything else in the security context. They can be set as bearer tokens in the HTTP "Authorization" header, as the value of the `access_token` URL parameter or in the cookie with the same name. At least the `sub` field should be present in the payload and it should be the username.

When a command causes an effect, the JWT payload will be saved along with the event as the `_jwt` field. Hence, all processing stages have access to the complete security context. From any point a reply can be sent back to the user, simply by placing a message on the user Kafka topic with the same name.

## Validators and Reducers

These are functions provided by the application. In both cases the signature is like this:

```
BiFunction<JsonObject, JsonObject, CompletionStage<JsonObject>>
```

The first argument is always the current state of the aggregate. The second is always the command. Since the functions return a completion stage, they are allowed to access other resources asynchronously. If they don't the resulting JSON can be wrapped in a completed future.

A validator should return the incoming command, potentially annotated with errors. The structure of these annotations is chosen by the application. The validator and the client should agree upon this. When there are errors then at least the boolean field `\_error` should be set.

A reducer is supposed to return the new state of the aggregate. The persistent actor will compare it with the current state and save the differences as an event, which is also published on a Kafka topic with the same name as in the `\_type` field.

## Kafka Consumers

You don't have to write Kafka consumers yourself. The library provides an Akka actor that behaves like a Kafka consumer in reliable mode. You only have to give it a function, which receives a message key and the message itself in JSON. The function should return a boolean to indicate success or failure in processing the message. The signature is like this:

```
BiFunction<String, JsonObject, CompletionStage<Boolean>>
```

When the function returns `true` the offset of the internal consumer is committed, otherwise it is rewound.

## Set-up

An application that wants to use the library will be in fact a Play application. It sets up a couple of routes to the generic controller and can add some more of its own. A routes file could look like this:

```
DELETE /api/:type/:id be.lemonade.jes.play.JsonAggregateController.delete(type: String, id: String)
GET /api/:type/:id be.lemonade.jes.play.JsonAggregateController.get(type: String, id: String)
GET /api/:type be.lemonade.jes.play.JsonAggregateController.searchMongo(type: String)
PATCH /api/:type/:id be.lemonade.jes.play.JsonAggregateController.patch(type: String, id: String)
POST /api/:type/:id be.lemonade.jes.play.JsonAggregateController.post(type: String, id: String)
PUT /api/:type/:id be.lemonade.jes.play.JsonAggregateController.put(type: String, id: String)
GET /api/ws be.lemonade.jes.play.JsonAggregateController.socket()
GET /api/sse be.lemonade.jes.play.JsonAggregateController.serverSentEvents()
```

An application comes to live through dependency injection. It starts with a Play module you have to define and configure. This is a sample module, which creates the `Registration` class:

```
package myapp;

import com.google.inject.AbstractModule;

public class Module extends AbstractModule {

  @Override
  protected void configure() {
    bind(Registration.class).asEagerSingleton();
  }
}
```

In the registration class you get the opportunity to register the validators, reducers and Kafka processors like this:

```
package myapp;

import be.lemonade.jes.akka.JsonProcessorRegistration;
import com.google.inject.Inject;

public class Registration {
  @Inject
  Registration(final JsonProcessorRegistration processors) {
    processors.setValidator("myapp-myaggregate", new MyAggregateValidator());
    processors.setReducer("myapp-myaggregate", new MyAggegrateReducer());
    processors.setConsumer("myapp-myaggregate-group", (key, message) -> true);    
  }
}
```

The validator will be something like in the following example:

```
package myapp;

import static be.lemonade.jes.akka.Commands.PUT;
import static be.lemonade.jes.akka.JsonFields.COMMAND;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static javax.json.Json.createObjectBuilder;

import be.lemonade.jes.akka.JsonProcessor;
import java.util.concurrent.CompletionStage;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class MyAggregateValidator implements JsonProcessor {
  private static JsonObject validate(final JsonObject command) {
    final JsonObjectBuilder builder = createObjectBuilder(command);
    ...
    return builder.build();
  }

  @Override
  public CompletionStage<JsonObject> apply(final JsonObject state,
    final JsonObject command) {
    switch (command.getString(COMMAND, "")) {
      case PUT:
        return completedFuture(validate(command));
      default:
        return completedFuture(command);
    }
  }
}
```

And the reducer would look like this:

```
package myapp;

import static be.lemonade.jes.akka.Commands.PUT;
import static be.lemonade.jes.akka.JsonFields.COMMAND;
import static be.lemonade.jes.akka.Util.delete;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static net.pincette.util.Collections.set;
import static net.pincette.util.Json.remove;

import be.lemonade.jes.akka.JsonProcessor;
import java.util.concurrent.CompletionStage;
import javax.json.JsonObject;

public class AggregateReducer implements JsonProcessor {
  @Override
  public CompletionStage<JsonObject> apply(final JsonObject state,
    final JsonObject command) {
    final JsonObject com = remove(command, set(COMMAND));

    switch (command.getString(COMMAND, "")) {
      case PUT:
        return completedFuture(com);
      case DELETE:
        return completedFuture(delete(state));
      default:
        return completedFuture(state);
    }
  }
}
```

## Search With MongoDB

Since we are working with JSON MongoDB is an interesting candidate for search. It indexes a JSON document completely and then you can use its powerful query language on everything in it.

The library comes with an indexer that can be registered as a Kafka consumer for an aggregate type. It will listen to all changes of the aggregates and update them in a collection with the same name as the aggregate type. It can be registered like this:

```
import be.lemonade.jes.akka.JsonProcessorRegistration;
import be.lemonade.jes.mongo.Indexer;
import com.google.inject.Inject;
import com.mongodb.reactivestreams.client.MongoClient;
import com.typesafe.config.Config;

public class Registration {
  @Inject
  Registration(
      final JsonProcessorRegistration processors,
      final MongoClient mongoClient,
      final Config config) {
    processors.setConsumer(
        "myaggregate",
        "myaggregate-group", // Kafka consumer group
        new Indexer(mongoClient, config));
  }
}
```

The constructor of the indexer has an optional fourth argument. It's a function that can transform the aggregate before it is sent to MongoDB. This is its signature:

```
UnaryOperator<JsonObject>
```

The generic Play controller has a method called `searchMongo` that expects the path parameter `type` (`:type` in the route), which corresponds to an aggregate type. When called without URL parameters it will return an array with all the aggregates of that type. The following URL parameters are supported:

|Name|Description|
|--|--|
|query|A JSON string in the [MongoDB query format](https://docs.mongodb.com/manual/reference/method/db.collection.find/#db.collection.find).|
|projection|A JSON string in the MongoDB projection format, which you can find on the same page.|
|pagesize|The maximum number of returned aggregates.|
|page|The page offset of the returned set. It starts with the value 1.|
|aggregate|A JSON array of MongoDB aggregation stages. When this parameter is present the value of `query` becomes the `$match` stage and the value of `projecvtion`, if present, becomes the `$project` stage, which preceeds `$match`. The given stages will go after `$match`. The parameter `page` will be translated in a `$skip` stage and `pagesize` will become a `$limit` stage.

It is also possible to do a POST with a JSON body, in which the fields ```query```, ```projection``` and ```aggregate``` are supported. They have the same structure as their URL parameter equivalents. When a field is present as both a URL parameter and in the body, the former will take precedence.

## Mutual References

Sometimes aggregates refer to each other in arrays. For example, an aggregate representing a person could have an array with organisations it is a member of, while an organisation could have an array with its members.

It is possible to keep this consistent in an automatic way by configuring two Kafka consumers. Each listens to the changes of its aggregate and sends commands to the affected aggregates on the other side. The class `be.lemonade.jes.kafka.ReferenceListConsumer` is a generic consumer that does this automatically. You can configure it in the registration class as follows:

```
import be.lemonade.jes.akka.JsonProcessorRegistration;
import be.lemonade.jes.kafka.ReferenceListConsumer;
import be.lemonade.jes.akka.JsonAggregateRootRef;

public class Registration {
  @Inject
  Registration(
      final JsonProcessorRegistration processors,
      final JsonAggregateRootRef rootRef) {
    processors.setConsumer(
        "myaggregate1",
        "myaggregate1-group", // Kafka consumer group
        new ReferenceListConsumer(
            "/references-myaggregate2",
            (id, href) -> createCommand("myaggregate2", id, "addAggregate1", href),
            (id, href) -> createCommand("myaggregate2", id, "removeAggregate1, href),
            rootRef));

    processors.setConsumer(
        "myaggregate2",
        "myaggregate2-group", // Kafka consumer group
        new ReferenceListConsumer(
            "/references-myaggregate1",
            (id, href) -> createCommand("myaggregate1", id, "addAggregate2", href),
            (id, href) -> createCommand("myaggregate1", id, "removeAggregate2, href),
            rootRef));
  }
}
```

The two lambdas create the commands to either add or remove a reference to the other type of aggregate. The signature is like this:

```
BiFunction<String, JsonValue, JsonObject>
```

## Configuration

A Play application always comes with a configuration. In there we have to set up the Akka cluster, Akka persistence, the Play modules and some specifics for this library. The following is an example with some inline comments. It uses [MongoDB](https://www.mongodb.com) for the persistence part.

```
akka {
  actor {
    provider = akka.cluster.ClusterActorRefProvider
    serializers {
      bson = be.lemonade.jes.akka.BsonSerializer
      kryo = com.romix.akka.serialization.kryo.KryoSerializer
    }
    serialization-bindings {
      "java.io.Serializable" = kryo
      "javax.json.JsonValue" = bson
    }
  }
  remote {
    log-remote-lifecycle-events = off
    netty.tcp {
      hostname = "localhost"
      port = 2554
    }
  }
  cluster {
    auto-down-unreachable-after = off
    seed-nodes = ["akka.tcp://sectorcrm@localhost:2554"]
  }
  contrib.persistence.mongodb.mongo {
    journal-collection = "journal"
    journal-index = "journal_index"
    snaps-collection = "snapshots"
    snaps-index = "snapshots_index"
    journal-write-concern = "Acknowledged"
    mongouri = "mongodb://localhost:27017/es"
  }
  extensions = [
    "com.romix.akka.serialization.kryo.KryoSerializationExtension$"]
  persistence {
    journal.plugin = "akka-contrib-mongodb-persistence-journal"
    snapshot-store.plugin = "akka-contrib-mongodb-persistence-snapshot"
  }
}

be.lemonade.jes {
  kafka {
    producer.client.id = "myapp"
    #Interval for non-blocking Kafka consumer in millis.
    pollInterval = 10
    #Time to wait after failed consumption in millis.
    backoffTime = 500
    command {
      #Group for the consumer that dispatches commands
      groupId = "myapp-command"
      #The command topic
      topic = "myapp-command"
    }
    config.bootstrap.servers = "localhost:9092"
  }
  mongodb { # When using MongoDB for search.
    uri = "mongodb://localhost:27017"
    database = "es"    
  }
  sns {
    sseTopic = "<AWS ARN>"
    region = "eu-central-1"    
  }  
  #The time in millis after which an actor is destroyed.
  passivationTimeout = 120000
  #The number of events before another snapshot is made.
  #Snapshots speed up the resurrection of an actor.
  snapshotInterval = 100
  #The public key to verify JSON Web Tokens.
  jwtRSAPublicKey = "MIIBIjANBgkqhki..."
}

play {
  akka.actor-system = "myapp"
  http.secret.key = "xxxxxxxxxxxxxxxxxxxxxxxxx"
  modules {
    #The library module
    enabled += be.lemonade.jes.akka.Module
    #The application module
    enabled += myapp.Module
  }
}

```

## Dependencies

For the build you need the following dependencies:

```
"com.github.romix.akka" %% "akka-kryo-serialization" % "0.5.2",
"org.glassfish" % "javax.json" % "1.1.4" withSources(),
"net.pincette" % "pincette-common" % "1.4" withSources(),
"be.lemonade" % "lemonade-jes" % "1.0.2" withSources(),
"be.lemonade" % "lemonade-jes-util" % "1.0" withSources(),
"io.netty" % "netty-handler" % "4.1.32.Final",
"com.typesafe.akka" %% "akka-persistence-dynamodb" % "1.1.1",
"org.mockito" % "mockito-core" % "2.23.4" % "test"
```

