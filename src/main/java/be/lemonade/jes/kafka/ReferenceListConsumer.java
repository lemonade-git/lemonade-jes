package be.lemonade.jes.kafka;

import static akka.pattern.Patterns.ask;
import static be.lemonade.jes.util.Aggregate.isDeleted;
import static be.lemonade.jes.util.JsonFields.AFTER;
import static be.lemonade.jes.util.JsonFields.BEFORE;
import static be.lemonade.jes.util.JsonFields.ID;
import static be.lemonade.jes.util.JsonFields.OPS;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.regex.Pattern.compile;

import akka.actor.ActorRef;
import be.lemonade.jes.akka.JsonAggregateRootRef;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;
import net.pincette.util.Json;
import scala.compat.java8.FutureConverters;

/**
 * This consumer listens to changes in a given list of references to another type of aggregate and
 * sends the configured commands to it with the source aggregate. This way the target aggregate can
 * become consistent with the source. This is useful when two types of aggregates maintain a list of
 * references to each other.
 *
 * @author Werner Donn\u00e9
 */
public class ReferenceListConsumer implements JsonConsumer {
  private static final String ADD = "add";
  private static final Pattern ARRAY_INDEX = compile("/\\d+");
  private static final String COPY = "copy";
  private static final String FROM = "from";
  private static final String MOVE = "move";
  private static final String OP = "op";
  private static final String PATH_FIELD = "path";
  private static final String REMOVE = "remove";
  private static final String VALUE = "value";

  private final BiFunction<String, JsonValue, JsonObject> addCommand;
  private final BiFunction<String, JsonValue, JsonObject> removeCommand;
  private final ActorRef dispatcher;
  private final String path;

  /**
   * Creates the consumer for the reference list denoted by <code>path</code>.
   *
   * @param path the path of the reference list.
   * @param addCommand the function that creates the command to add a reference. The argument is the
   *     ID of the aggregate that changed. The second argument is the value that denotes the
   *     aggregate that should be changed.
   * @param removeCommand the function that creates the command to remove a reference. The argument
   *     is the ID of the aggregate that changed. The second argument is the value that denotes the
   *     aggregate that should be changed.
   * @param dispatcher this is where the commands are sent to.
   */
  public ReferenceListConsumer(
      final String path,
      final BiFunction<String, JsonValue, JsonObject> addCommand,
      final BiFunction<String, JsonValue, JsonObject> removeCommand,
      final JsonAggregateRootRef dispatcher) {
    this.path = path;
    this.addCommand = addCommand;
    this.removeCommand = removeCommand;
    this.dispatcher = dispatcher.actorRef();
  }

  private static Stream<JsonObject> createCommands(
      final JsonObject aggregate,
      final String referenceListPath,
      final BiFunction<String, JsonValue, JsonObject> create) {
    return net.pincette.util.Json.getValue(aggregate, referenceListPath)
        .filter(Json::isArray)
        .map(JsonValue::asJsonArray)
        .map(JsonArray::stream)
        .map(s -> s.map(v -> create.apply(aggregate.getString(ID), v)))
        .orElseGet(Stream::empty);
  }

  private static Optional<Stream<JsonObject>> getOps(final JsonObject message) {
    return Optional.ofNullable(message.getJsonArray(OPS))
        .map(JsonArray::stream)
        .map(s -> s.filter(Json::isObject).map(JsonValue::asJsonObject));
  }

  private static JsonValue getValue(final JsonObject op, final JsonObject before) {
    final String name = op.getString(OP);
    final Supplier<String> field = () -> REMOVE.equals(name) ? PATH_FIELD : FROM;

    return ADD.equals(name) ? op.getValue("/" + VALUE) : before.getValue(op.getString(field.get()));
  }

  private static boolean isAddOfHigherPath(final JsonObject op, final String path) {
    return ADD.equals(op.getString(OP))
        && Optional.of(op.getString(PATH_FIELD)).map(path::startsWith).orElse(false);
  }

  private static boolean isNew(final JsonObject message, final String path) {
    return getOps(message).map(s -> s.anyMatch(op -> isAddOfHigherPath(op, path))).orElse(false);
  }

  @Override
  public CompletionStage<Boolean> apply(String key, JsonObject message) {
    final Supplier<CompletionStage<Boolean>> newOr =
        () -> isNew(message, path) ? handleNew(message) : handleOps(message);

    return isDeleted(message.getJsonObject(AFTER)) ? handleDeleted(message) : newOr.get();
  }

  private JsonObject createCommand(final JsonObject op, final JsonObject message) {
    return Optional.of(op.getString(OP))
        .filter(o -> o.equals(REMOVE))
        .map(o -> removeCommand)
        .orElse(addCommand)
        .apply(message.getString(ID), getValue(op, message.getJsonObject(BEFORE)));
  }

  private CompletionStage<Boolean> dispatch(final JsonObject command) {
    return Optional.ofNullable(command)
        .map(
            c ->
                FutureConverters.toJava(ask(dispatcher, c, 5000))
                    .thenApply(reply -> (JsonObject) reply)
                    .thenApply(reply -> true))
        .orElse(completedFuture(true));
  }

  private CompletionStage<Boolean> dispatchCommands(final Stream<JsonObject> commands) {
    return commands
        .map(this::dispatch)
        .reduce(
            completedFuture(true),
            (c, d) -> c.thenComposeAsync(result -> result ? d : completedFuture(false)),
            (c1, c2) -> c1);
  }

  private CompletionStage<Boolean> handleDeleted(final JsonObject message) {
    return dispatchCommands(createCommands(message.getJsonObject(AFTER), path, removeCommand));
  }

  private CompletionStage<Boolean> handleNew(final JsonObject message) {
    return dispatchCommands(createCommands(message.getJsonObject(AFTER), path, addCommand));
  }

  private CompletionStage<Boolean> handleOps(final JsonObject message) {
    return getOps(message)
        .map(s -> dispatchCommands(s.filter(this::opMatches).map(op -> createCommand(op, message))))
        .orElse(completedFuture(false));
  }

  private boolean opMatches(final JsonObject op) {
    return pathMatches(op)
        && Optional.ofNullable(op.getString(OP, null))
            .filter(
                o ->
                    ADD.equals(o)
                        || REMOVE.equals(o)
                        || Optional.ofNullable(op.getString(FROM, null))
                            .filter(from -> opSourceMatches(o, from, op.getString(PATH_FIELD)))
                            .isPresent())
            .isPresent();
  }

  private boolean opSourceMatches(final String op, final String from, final String to) {
    return (COPY.equals(op) || MOVE.equals(op))
        && (!pathMatches(from) || (COPY.equals(op) && !from.equals(to)));
  }

  private boolean pathMatches(final JsonObject op) {
    return Optional.ofNullable(op.getString(PATH_FIELD, null))
        .filter(this::pathMatches)
        .isPresent();
  }

  private boolean pathMatches(final String path) {
    return path.startsWith(this.path)
        && ARRAY_INDEX.matcher(path.substring(this.path.length())).matches();
  }
}
