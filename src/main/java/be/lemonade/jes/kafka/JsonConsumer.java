package be.lemonade.jes.kafka;

import java.util.concurrent.CompletionStage;
import java.util.function.BiFunction;
import javax.json.JsonObject;

/**
 * The first argument is the message key. The second one is the message. The implementation should
 * return <code>true</code> is processing of the message was successful and <code>false</code>
 * otherwise. In the latter case the offset will not be committed.
 *
 * @author Werner Donn\u00e9
 */
@FunctionalInterface
public interface JsonConsumer extends BiFunction<String, JsonObject, CompletionStage<Boolean>> {}
