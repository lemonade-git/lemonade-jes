package be.lemonade.jes.kafka;

import static be.lemonade.jes.util.Kafka.getConfig;
import static be.lemonade.jes.util.Logging.LOGGER;
import static java.lang.Long.MAX_VALUE;
import static java.lang.String.join;
import static java.time.Duration.ofMillis;
import static java.time.Instant.now;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.logging.Level.INFO;
import static java.util.logging.Level.SEVERE;
import static java.util.logging.Logger.getLogger;
import static net.pincette.util.Collections.map;
import static net.pincette.util.Collections.merge;
import static net.pincette.util.Pair.pair;
import static net.pincette.util.StreamUtil.stream;
import static net.pincette.util.Util.getStackTrace;
import static net.pincette.util.Util.tryToGet;
import static net.pincette.util.Util.tryToGetSilent;

import akka.actor.AbstractActor;
import akka.actor.PoisonPill;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import net.pincette.function.SideEffect;
import net.pincette.util.Pair;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;

/**
 * This is a Kafka consumer wrapped in an Akka actor. The consumer will commit the offset only when
 * the given processor returns <code>true</code>.
 *
 * @param <K> the key type.
 * @param <V> the value type.
 * @author Werner Donn\u00e9
 */
public class ReliableConsumerActor<K, V> extends AbstractActor {
  private static final String POLL = "poll";

  private final Duration backoffTime;
  private final Config<K, V> config;
  private final long consumerMaxAge;
  private final Map<String, Object> kafkaProperties;
  private final Duration pollInterval;
  private final Duration pollTimeout;
  private final BiFunction<K, V, CompletionStage<Boolean>> processor;
  private final Set<String> topics;
  private KafkaConsumer<K, V> consumer;
  private Instant consumerBirth = now();
  private long polls;
  private boolean recentlyReceived;

  /**
   * Creates the consumer and subscribes to it.
   *
   * @param topics the topics that should be consumed.
   */
  public ReliableConsumerActor(final Set<String> topics, final Config<K, V> config) {
    this.topics = topics;
    this.config = config;
    this.processor = config.processor;
    pollInterval =
        tryToGetSilent(() -> ofMillis(config.configuration.getLong("kafka.pollInterval")))
            .orElse(ofMillis(10));
    pollTimeout =
        tryToGetSilent(() -> ofMillis(config.configuration.getLong("kafka.pollTimeout")))
            .orElse(ofMillis(MAX_VALUE));
    backoffTime =
        tryToGetSilent(() -> ofMillis(config.configuration.getLong("kafka.backoffTime")))
            .orElse(ofMillis(500));
    consumerMaxAge =
        tryToGetSilent(() -> config.configuration.getLong("kafka.consumerMaxAge")).orElse(300000L);
    kafkaProperties =
        merge(
            getConfig(config.configuration, "kafka.config"),
            config.extraKafkaConfig,
            map(pair("enable.auto.commit", false)));
  }

  private static Map<TopicPartition, OffsetAndMetadata> getOffset(final ConsumerRecord record) {
    return map(
        pair(
            new TopicPartition(record.topic(), record.partition()),
            new OffsetAndMetadata(record.offset() + 1)));
  }

  private boolean commit(final ConsumerRecord record) {
    return tryToGet(
            () -> {
              consumer.commitSync(getOffset(record));

              return true;
            })
        .orElseGet(() -> commitFailed(record));
  }

  private boolean commitFailed(final ConsumerRecord record) {
    getLogger(LOGGER)
        .log(
            SEVERE,
            "Group {0}: commit of record {1} of topic {2} failed",
            new Object[] {getGroupId(), record.key(), record.topic()});

    return false;
  }

  public Receive createReceive() {
    return receiveBuilder().matchEquals(POLL, message -> handlePoll()).build();
  }

  private KafkaConsumer<K, V> getConsumer() {
    if (consumer == null
        || (!recentlyReceived && now().minusMillis(consumerMaxAge).isAfter(consumerBirth))) {
      if (consumer != null) {
        consumer.close();
      }

      consumer = new KafkaConsumer<>(kafkaProperties);
      consumer.subscribe(topics);
      consumerBirth = now();
    }

    return consumer;
  }

  private String getGroupId() {
    return Optional.ofNullable(kafkaProperties.get("group.id"))
        .map(id -> (String) id)
        .orElse("unknown");
  }

  private void handlePoll() {
    next(
        tryToGet(
                () ->
                    stream(getConsumer().poll(pollTimeout).iterator())
                        .map(this::markRecentyReceived)
                        .map(this::log)
                        .map(this::processRecord)
                        .reduce(
                            (c1, c2) ->
                                c1.thenComposeAsync(
                                    result -> result.second ? c2 : completedFuture(result)))
                        .flatMap(stage -> tryToGet(() -> stage.toCompletableFuture().get()))
                        .map(this::logResult)
                        .map(this::processResult)
                        .orElseGet(
                            () ->
                                SideEffect.<Duration>run(() -> recentlyReceived = false)
                                    .andThenGet(() -> pollInterval)),
                e ->
                    SideEffect.<Duration>run(() -> getLogger(LOGGER).severe(getStackTrace(e)))
                        .andThenGet(() -> null))
            .orElseGet(
                () ->
                    SideEffect.<Duration>run(() -> recentlyReceived = false)
                        .andThenGet(() -> backoffTime)));
  }

  private ConsumerRecord<K, V> log(final ConsumerRecord<K, V> record) {
    getLogger(LOGGER)
        .log(
            INFO,
            "Group {0}: consuming record {1} for topic {2}",
            new Object[] {getGroupId(), record.key(), record.topic()});

    return record;
  }

  private Pair<ConsumerRecord<K, V>, Boolean> logResult(
      final Pair<ConsumerRecord<K, V>, Boolean> result) {
    getLogger(LOGGER)
        .log(
            result.second ? INFO : SEVERE,
            "Group {0}: processing record {1} of topic {2} {3}",
            new Object[] {
              getGroupId(),
              result.first.key(),
              result.first.topic(),
              result.second ? "succeeded" : "failed"
            });

    return result;
  }

  private ConsumerRecord<K, V> markRecentyReceived(final ConsumerRecord<K, V> record) {
    recentlyReceived = true;

    return record;
  }

  private void maybeStop() {
    if (config.stopOnFalse) {
      self().tell(PoisonPill.getInstance(), self());
    }
  }

  private void next(final Duration duration) {
    context()
        .system()
        .scheduler()
        .scheduleOnce(
            scala.concurrent.duration.Duration.create(duration.toMillis(), TimeUnit.MILLISECONDS),
            this::poll,
            context().system().dispatcher());
  }

  private void poll() {
    if (polls % 1000 == 0) {
      getLogger(LOGGER)
          .log(
              INFO,
              "Group {0}: polling topics {1}",
              new Object[] {getGroupId(), join(", ", topics)});
    }

    self().tell(POLL, self());
    ++polls;
  }

  @Override
  public void postStop() {
    getLogger(LOGGER)
        .log(
            INFO,
            "Group {0}: kill consumer for topics {1}",
            new Object[] {getGroupId(), join(", ", topics)});

    if (consumer != null) {
      consumer.close();
    }
  }

  @Override
  public void preStart() {
    poll();
  }

  private CompletionStage<Pair<ConsumerRecord<K, V>, Boolean>> processRecord(
      final ConsumerRecord<K, V> record) {
    return processor
        .apply(record.key(), record.value())
        .thenApply(result -> pair(record, result))
        .exceptionally(
            e -> {
              getLogger(LOGGER).severe(getStackTrace(e));
              return pair(record, false);
            });
  }

  private Duration processResult(final Pair<ConsumerRecord<K, V>, Boolean> result) {
    return !result.second || !commit(result.first)
        ? SideEffect.<Duration>run(
                () -> {
                  rewind(result.first);
                  maybeStop();
                })
            .andThenGet(() -> backoffTime)
        : pollInterval;
  }

  private void rewind(final ConsumerRecord record) {
    consumer.seek(new TopicPartition(record.topic(), record.partition()), record.offset());
  }

  public static class Config<K, V> {
    private final com.typesafe.config.Config configuration;
    private final Map<String, Object> extraKafkaConfig;
    private final BiFunction<K, V, CompletionStage<Boolean>> processor;
    private final boolean stopOnFalse;

    /**
     * The configuration needed to create the actor.
     *
     * @param processor the function that should return <code>true</code> in order for the offset to
     *     be committed.
     * @param config the Kafka configuration. The path "kafka.pollInterval" is the interval in
     *     milliseconds between two polls. At the Kafka level the poll will have no timeout and
     *     hence return immediately. This avoids blocking. The <code>pollInterval</code> is the
     *     maximum latency one is prepared to accept. The default value is 10ms. The path
     *     "kafka.backoffTime" is the time in milliseconds after a message processing failure. The
     *     default value is 500ms. The path "kafka.config" is the general Kafka configuration.
     * @param extraKafkaConfig extra configurations for Kafka.
     */
    public Config(
        final BiFunction<K, V, CompletionStage<Boolean>> processor,
        final com.typesafe.config.Config config,
        final Map<String, Object> extraKafkaConfig,
        final boolean stopOnFalse) {
      this.processor = processor;
      this.configuration = config;
      this.extraKafkaConfig = extraKafkaConfig != null ? extraKafkaConfig : new HashMap<>();
      this.stopOnFalse = stopOnFalse;
    }
  }
}
