package be.lemonade.jes.akka;

import static akka.pattern.Patterns.ask;
import static be.lemonade.jes.util.Command.createCommandBuilder;
import static be.lemonade.jes.util.Commands.GET;
import static be.lemonade.jes.util.Event.isEvent;
import static be.lemonade.jes.util.Event.isNext;
import static be.lemonade.jes.util.Event.sequenceErrorMessage;
import static be.lemonade.jes.util.JsonFields.AFTER;
import static be.lemonade.jes.util.JsonFields.BEFORE;
import static be.lemonade.jes.util.JsonFields.COMMAND;
import static be.lemonade.jes.util.JsonFields.CORR;
import static be.lemonade.jes.util.JsonFields.ID;
import static be.lemonade.jes.util.JsonFields.JWT;
import static be.lemonade.jes.util.JsonFields.LANGUAGES;
import static be.lemonade.jes.util.JsonFields.OPS;
import static be.lemonade.jes.util.JsonFields.SEQ;
import static be.lemonade.jes.util.JsonFields.TEST;
import static be.lemonade.jes.util.JsonFields.TYPE;
import static be.lemonade.jes.util.Util.isManagedObject;
import static java.util.UUID.randomUUID;
import static javax.json.Json.createArrayBuilder;
import static javax.json.Json.createDiff;
import static javax.json.Json.createObjectBuilder;
import static javax.json.Json.createPatch;
import static net.pincette.util.Builder.create;
import static net.pincette.util.Collections.set;
import static net.pincette.util.Json.addIf;
import static net.pincette.util.Json.addTransformer;
import static net.pincette.util.Json.asArray;
import static net.pincette.util.Json.getValue;
import static net.pincette.util.Json.isArray;
import static net.pincette.util.Json.nopTransformer;
import static net.pincette.util.Json.toDotSeparated;
import static net.pincette.util.Json.transform;
import static net.pincette.util.Util.must;
import static net.pincette.util.Util.pathSearch;

import akka.actor.ActorRef;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import net.pincette.function.ConsumerWithException;
import net.pincette.util.Json;
import net.pincette.util.Json.JsonEntry;
import net.pincette.util.Json.Transformer;
import scala.compat.java8.FutureConverters;

/**
 * Utilities.
 *
 * @author Werner Donn\u00e9
 */
public class Util {
  public static final String HREF = "href";
  private static final Set<String> TECHNICAL_FIELDS =
      set(COMMAND, CORR, ID, JWT, LANGUAGES, SEQ, TEST, TYPE);

  private Util() {}

  /**
   * Add an object with the field <code>href</code> to the array denoted by <code>path</code>, which
   * can be with slashed or dots. If the object is already in the array it will not be added again.
   *
   * @param json the JSON object.
   * @param path the path of the array in the JSON object.
   * @param href the URL.
   * @return The modified JSON object.
   */
  public static JsonObject addReference(
      final JsonObject json, final String path, final String href) {
    final String withDots = toDotSeparated(path);

    return pathSearch(json, withDots + "[href='" + href + "'].href", Json::evaluate)
        .map(o -> json)
        .orElseGet(
            () ->
                transform(
                    json,
                    getValue(json, path)
                        .map(p -> nopTransformer())
                        .orElse(addTransformer(withDots, createArrayBuilder().build()))
                        .thenApply(
                            new Transformer(
                                e -> e.path.equals(withDots) && isArray(e.value),
                                e ->
                                    Optional.of(
                                        new JsonEntry(
                                            e.path,
                                            createArrayBuilder(asArray(e.value))
                                                .add(createObjectBuilder().add(HREF, href))
                                                .build()))))));
  }

  /**
   * Applies <code>event</code> to <code>json</code> if both denote the same managed object, i.e.
   * they have the same <code>_type</code> and <code>_id</code> fields. The value of the <code>_seq
   * </code> field in <code>event</code> should one higher than the same field in <code>json</code>,
   * otherwise <code>report</code> will be called.
   *
   * @param json the JSON object.
   * @param event the event.
   * @param report the function to report sequence errors.
   * @return The new JSON object.
   */
  public static JsonObject applyEvent(
      final JsonObject json, final JsonObject event, final ConsumerWithException<String> report) {
    final ConsumerWithException<JsonObject> reporter =
        report != null ? (e -> report.accept(sequenceErrorMessage(json, e))) : null;

    return !isEvent(event) || !sameManagedObject(json, event) || seen(json, event)
        ? json
        : createObjectBuilder(
                createPatch(must(event, e -> isNext(json, e), reporter).getJsonArray(OPS))
                    .apply(json))
            .add(SEQ, event.getInt(SEQ))
            .build();
  }

  private static JsonObjectBuilder createAfter(
      final JsonObject newState, final JsonObject command, final String corr, final int seq) {
    return create(() -> createObjectBuilder(newState).add(CORR, corr).add(SEQ, seq).remove(JWT))
        .updateIf(b -> command.containsKey(JWT), b -> b.add(JWT, command.getJsonObject(JWT)))
        .build();
  }

  public static JsonObject createEvent(
      final JsonObject oldState, final JsonObject newState, final JsonObject command) {
    final String corr = command.getString(CORR, randomUUID().toString());
    final int seq = oldState.getInt(SEQ, -1) + 1;

    return create(
            () ->
                createObjectBuilder()
                    .add(CORR, corr)
                    .add(ID, newState.getString(ID).toLowerCase())
                    .add(TYPE, newState.getString(TYPE))
                    .add(SEQ, seq)
                    .add(BEFORE, oldState)
                    .add(AFTER, createAfter(newState, command, corr, seq))
                    .add(OPS, createOps(oldState, newState)))
        .updateIf(
            b -> command.containsKey(LANGUAGES),
            b -> b.add(LANGUAGES, command.getJsonArray(LANGUAGES)))
        .updateIf(b -> command.containsKey(JWT), b -> b.add(JWT, command.getJsonObject(JWT)))
        .build()
        .build();
  }

  private static JsonArrayBuilder createOps(final JsonObject oldState, final JsonObject newState) {
    return createArrayBuilder(
        createDiff(removeTechnical(oldState).build(), removeTechnical(newState).build())
            .toJsonArray());
  }

  /**
   * Gets an aggregate directly from the dispatcher.
   *
   * @param type the aggregate type.
   * @param id the ID of the aggregate instance.
   * @param jwt the JSON Web Token payload. It may be <code>null</code>.
   * @param dispatcher the dispatcher actor.
   * @return The aggregate.
   */
  public static CompletionStage<JsonObject> getInternal(
      final String type, final String id, final JsonObject jwt, final ActorRef dispatcher) {
    return FutureConverters.toJava(
            ask(
                dispatcher,
                addIf(createCommandBuilder(type, id, GET), () -> jwt != null, b -> b.add(JWT, jwt))
                    .build(),
                5000))
        .thenApply(reply -> (JsonObject) reply);
  }

  public static boolean isTest(final JsonObject command) {
    return command.getBoolean(TEST, false);
  }

  /**
   * Removes an object with the field <code>href</code> from the array denoted by <code>path</code>,
   * which can be with slashed or dots.
   *
   * @param json the JSON object.
   * @param path the path of the array in the JSON object.
   * @param href the URL.
   * @return The modified JSON object.
   */
  public static JsonObject removeReference(
      final JsonObject json, final String path, final String href) {
    final String withDots = toDotSeparated(path);

    return transform(
        json,
        new Transformer(
            e -> e.path.equals(withDots) && isArray(e.value),
            e -> Optional.of(new JsonEntry(e.path, removeReference(e.value.asJsonArray(), href)))));
  }

  private static JsonArray removeReference(final JsonArray array, final String href) {
    return array.stream()
        .filter(Json::isObject)
        .map(JsonValue::asJsonObject)
        .filter(json -> json.containsKey(HREF))
        .filter(json -> !json.getString(HREF).equals(href))
        .reduce(createArrayBuilder(), JsonArrayBuilder::add, (b1, b2) -> b1)
        .build();
  }

  static JsonObjectBuilder removeTechnical(final JsonObject json) {
    return TECHNICAL_FIELDS.stream()
        .reduce(createObjectBuilder(json), JsonObjectBuilder::remove, (b1, b2) -> b1);
  }

  public static boolean sameManagedObject(final JsonObject json1, final JsonObject json2) {
    return isManagedObject(json1)
        && isManagedObject(json2)
        && json1.getString(ID).equalsIgnoreCase(json2.getString(ID))
        && json1.getString(TYPE).equals(json2.getString(TYPE));
  }

  private static boolean seen(final JsonObject json, final JsonObject event) {
    return event.getInt(SEQ) <= json.getInt(SEQ);
  }
}
