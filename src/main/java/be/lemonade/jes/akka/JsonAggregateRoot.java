package be.lemonade.jes.akka;

import static be.lemonade.jes.akka.Util.createEvent;
import static be.lemonade.jes.akka.Util.isTest;
import static be.lemonade.jes.akka.Util.removeTechnical;
import static be.lemonade.jes.aws.Util.sendNotification;
import static be.lemonade.jes.util.Aggregate.isDeleted;
import static be.lemonade.jes.util.Command.isCommand;
import static be.lemonade.jes.util.Commands.GET;
import static be.lemonade.jes.util.Commands.PASSIVATE;
import static be.lemonade.jes.util.Commands.PUT;
import static be.lemonade.jes.util.Event.isEvent;
import static be.lemonade.jes.util.JsonFields.AFTER;
import static be.lemonade.jes.util.JsonFields.BEFORE;
import static be.lemonade.jes.util.JsonFields.COMMAND;
import static be.lemonade.jes.util.JsonFields.CORR;
import static be.lemonade.jes.util.JsonFields.ERROR;
import static be.lemonade.jes.util.JsonFields.ID;
import static be.lemonade.jes.util.JsonFields.JWT;
import static be.lemonade.jes.util.JsonFields.LANGUAGES;
import static be.lemonade.jes.util.JsonFields.OPS;
import static be.lemonade.jes.util.JsonFields.SEQ;
import static be.lemonade.jes.util.JsonFields.STATUS_CODE;
import static be.lemonade.jes.util.JsonFields.TEST;
import static be.lemonade.jes.util.JsonFields.TYPE;
import static be.lemonade.jes.util.Kafka.createReliableProducer;
import static be.lemonade.jes.util.Kafka.getProducerConfig;
import static be.lemonade.jes.util.Logging.LOGGER;
import static be.lemonade.jes.util.Logging.log;
import static java.lang.Integer.MAX_VALUE;
import static java.time.Duration.between;
import static java.time.Instant.now;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.concurrent.CompletableFuture.runAsync;
import static java.util.logging.Level.INFO;
import static java.util.logging.Logger.getLogger;
import static java.util.stream.Collectors.toSet;
import static javax.json.Json.createArrayBuilder;
import static javax.json.Json.createObjectBuilder;
import static javax.json.Json.createPatch;
import static net.pincette.util.Collections.set;
import static net.pincette.util.Json.addIf;
import static net.pincette.util.Json.copy;
import static net.pincette.util.Json.emptyObject;
import static net.pincette.util.Json.get;
import static net.pincette.util.Json.getValue;
import static net.pincette.util.Json.hasErrors;
import static net.pincette.util.Json.remove;
import static net.pincette.util.Json.string;
import static net.pincette.util.Json.transform;
import static net.pincette.util.Pair.pair;
import static net.pincette.util.StreamUtil.rangeInclusive;
import static net.pincette.util.StreamUtil.zip;
import static net.pincette.util.Util.allPaths;
import static net.pincette.util.Util.from;
import static net.pincette.util.Util.getStackTrace;
import static net.pincette.util.Util.tryToDoWithRethrow;
import static net.pincette.util.Util.tryToGet;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.cluster.sharding.ShardRegion;
import akka.persistence.AbstractPersistentActor;
import akka.persistence.SnapshotOffer;
import com.typesafe.config.Config;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Stream;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import net.pincette.util.Json;
import net.pincette.util.Json.JsonEntry;
import net.pincette.util.Util.GeneralException;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import software.amazon.awssdk.services.sns.SnsAsyncClient;

/**
 * An actor that manages aggregate roots which are expressed in JSON.
 *
 * @author Werner Donn\u00e9
 */
public class JsonAggregateRoot extends AbstractPersistentActor {
  private static final Set<String> COMMAND_TECHNICAL = set(COMMAND, LANGUAGES);
  private static final String IDLE = "idle";
  private static final String OP = "op";
  private static final String PATH = "path";

  private final String aggregateTopicSuffix;
  private final Duration passivationTimeout;
  private final Map<String, Object> producerConfig;
  private final JsonProcessor reduce;
  private final int snapshotInterval;
  private final SnsAsyncClient sns;
  private final String snsTopic;
  private final String type;
  private final JsonProcessor validate;
  private JsonObject state = emptyObject();
  private Instant touched = now();

  public JsonAggregateRoot(
      final String type,
      final JsonProcessor validate,
      final JsonProcessor reduce,
      final SnsAsyncClient sns,
      final Config config) {
    this.type = type;
    this.validate = validate;
    this.reduce = reduce;
    this.sns = sns;
    producerConfig = getProducerConfig(config);
    passivationTimeout =
        config.hasPath("be.lemonade.jes.passivationTimeout")
            ? Duration.ofMillis(config.getLong("be.lemonade.jes.passivationTimeout"))
            : null;
    snapshotInterval =
        config.hasPath("be.lemonade.jes.snapshotInterval")
            ? config.getInt("be.lemonade.jes.snapshotInterval")
            : 100;
    aggregateTopicSuffix =
        config.hasPath("be.lemonade.jes.kafka.aggregateTopicSuffix")
            ? config.getString("be.lemonade.jes.kafka.aggregateTopicSuffix")
            : "";
    snsTopic = config.getString("be.lemonade.jes.sns.sseTopic");
  }

  private static boolean appliesToEmbedded(final JsonObject op, final JsonObject embedded) {
    switch (op.getString(OP)) {
      case "remove":
      case "replace":
      case "test":
        return hasPath(embedded, op, PATH);
      case "copy":
      case "move":
        return hasPath(embedded, op, "from");
      default:
        return true;
    }
  }

  private static boolean changes(final JsonObject event) {
    return !event.getJsonArray(OPS).isEmpty();
  }

  private static JsonObject checkEvent(
      final JsonObject oldState, final Set<String> idPaths, final JsonObject event) {
    return Optional.of(event.getJsonArray(OPS).stream())
        .filter(s -> s.noneMatch(op -> isEmbeddedUpdate(idPaths, op.asJsonObject())))
        .map(s -> event)
        .orElseThrow(
            () ->
                new GeneralException(
                    "Embedded update of " + event.getString(ID) + " in " + oldState.getString(ID)));
  }

  private static KafkaProducer<String, String> createProducer(final Map<String, Object> config) {
    return createReliableProducer(config, new StringSerializer(), new StringSerializer());
  }

  private static boolean hasId(final Set<String> idPaths, final String path) {
    return allPaths(path, "/")
        .map(JsonAggregateRoot::siblingId)
        .anyMatch(id -> id.map(idPaths::contains).orElse(false));
  }

  private static boolean hasPath(final JsonObject json, final JsonObject op, final String path) {
    return get(json, toDots(op.getString(path))).isPresent();
  }

  private static boolean isEmbeddedUpdate(final Set<String> idPaths, final JsonObject op) {
    return !"test".equals(op.getString(OP)) && hasId(idPaths, op.getString(PATH));
  }

  private static boolean isFirst(final JsonObject event) {
    return event.getInt(SEQ) == 0;
  }

  private static boolean isPassivate(final JsonObject command) {
    return Optional.ofNullable(command.getString(COMMAND, null))
        .map(c -> c.equals(PASSIVATE))
        .orElse(false);
  }

  private static boolean isUsedEmbedded(final JsonValue value, final JsonObject event) {
    return Optional.of(value)
        .filter(Json::isObject)
        .map(JsonValue::asJsonObject)
        .filter(o -> Util.sameManagedObject(o, event))
        .isPresent();
  }

  private static ProducerRecord<String, String> record(final JsonObject json, final String topic) {
    return new ProducerRecord<>(topic, json.getString(ID), string(json));
  }

  private static JsonObject reduceEvent(final JsonObject event, final JsonObject embedded) {
    return createObjectBuilder(event)
        .add(
            OPS,
            copy(
                event.getJsonArray(OPS),
                createArrayBuilder(),
                op -> appliesToEmbedded(op.asJsonObject(), embedded)))
        .build();
  }

  private static JsonObject removeStates(final JsonObject json) {
    return createObjectBuilder(json).remove(BEFORE).remove(AFTER).build();
  }

  private static JsonObject setTest(final JsonObject event, final JsonObject command) {
    return addIf(
            createObjectBuilder(event),
            () -> command.containsKey(TEST),
            b -> b.add(TEST, command.getBoolean(TEST)))
        .build();
  }

  private static Optional<String> siblingId(final String path) {
    return Optional.of(path.lastIndexOf('/'))
        .filter(index -> index > 0) // The path "/_id" refers to the object itself.
        .map(index -> path.substring(0, index + 1) + ID);
  }

  private static String toDots(final String path) {
    return path.substring(1).replace('/', '.');
  }

  private static Stream<String> combine(final String parent, final Stream<String> children) {
    return children.map(child -> parent + child);
  }

  private static Stream<String> getEmbeddedIdPaths(final JsonValue json) {
    final Supplier<Stream<String>> arrayOf =
        () -> json instanceof JsonArray ? getEmbeddedIdPaths(json.asJsonArray()) : Stream.empty();

    return json instanceof JsonObject ? getEmbeddedIdPaths(json.asJsonObject()) : arrayOf.get();
  }

  private static Stream<String> getEmbeddedIdPaths(final JsonObject json) {
    return json.entrySet().stream()
        .flatMap(e -> pathWithCombinations(e.getKey(), e.getValue()))
        .filter(path -> path.endsWith(ID));
  }

  private static Stream<String> getEmbeddedIdPaths(final JsonArray json) {
    return zip(rangeInclusive(0, MAX_VALUE), json.stream())
        .flatMap(pair -> pathWithCombinations(String.valueOf(pair.first), pair.second));
  }

  private static Stream<String> pathWithCombinations(final String field, final JsonValue value) {
    return Stream.concat(Stream.of("/" + field), combine("/" + field, getEmbeddedIdPaths(value)));
  }

  private static boolean isNext(final JsonObject object, final JsonObject event) {
    return event.getInt(SEQ) == object.getInt(SEQ) + 1;
  }

  private static JsonObject setNotFound(final JsonObject command) {
    return createObjectBuilder(command).add(ERROR, true).add(STATUS_CODE, 404).build();
  }

  private static void throwCorruptEvent(final String id, final JsonObject event) {
    throw new GeneralException("Corrupt event " + string(event) + " for ID " + id);
  }

  private static JsonObject validateEvent(final JsonObject object, final JsonObject event) {
    if (!object.getString(ID).equalsIgnoreCase(event.getString(ID)) && !isNext(object, event)) {
      throwCorruptEvent(object.getString(ID), event);
    }

    return event;
  }

  private String aggregateTopic(final JsonObject message) {
    return message.getString(TYPE) + aggregateTopicSuffix;
  }

  private CompletionStage<JsonObject> applyBusinessLogic(final JsonObject command) {
    return validate
        .apply(state, command)
        .thenComposeAsync(
            json ->
                hasErrors(json)
                    ? completedFuture(json)
                    : reduce
                        .apply(state, command)
                        .thenApply(newState -> createObjectBuilder(newState).remove(TEST).build()));
  }

  private CompletionStage<JsonObject> applyEmbeddedUpdate(final JsonObject event) {
    return completedFuture(
        transform(
            state,
            new Json.Transformer(
                entry -> isUsedEmbedded(entry.value, event), entry -> applyEvent(entry, event))));
  }

  private Optional<JsonEntry> applyEvent(final JsonEntry entry, final JsonObject event) {
    final JsonObject object = entry.value.asJsonObject();

    return Optional.of(
        new Json.JsonEntry(
            entry.path,
            Util.applyEvent(
                object,
                reduceEvent(validateEvent(object, event), object),
                message -> context().system().log().error(message))));
  }

  public Receive createReceive() {
    return receiveBuilder()
        .matchEquals(IDLE, message -> handleIdle())
        .match(JsonObject.class, this::handleCommand)
        .build();
  }

  public Receive createReceiveRecover() {
    return receiveBuilder()
        .match(SnapshotOffer.class, this::handleSnapshot)
        .match(String.class, this::update)
        .build();
  }

  private Optional<String> getUser(final JsonObject json) {
    return Optional.ofNullable(json.getJsonObject(JWT)).flatMap(be.lemonade.jes.play.Util::getUser);
  }

  private void handleCommand(final JsonObject command) {
    log("Handle", command);

    if (!isCommand(command, PUT) && (state.isEmpty() || isDeleted(state))) {
      replyNotFound(command);
    } else if (isPassivate(command)) {
      passivate(sender());
    } else if (isEvent(command) || matches(command)) {
      touched = now();

      handleCommand(command, sender())
          .filter(stage -> !isCommand(command, GET))
          .ifPresent(stage -> stage.toCompletableFuture().join());
    }
  }

  private Optional<CompletionStage<Void>> handleCommand(
      final JsonObject command, final ActorRef sender) {
    return tryToGet(
        () ->
            (isEvent(command) ? applyEmbeddedUpdate(command) : applyBusinessLogic(command))
                .thenComposeAsync(
                    json ->
                        isCommand(command, GET) || hasErrors(json)
                            ? runAsync(() -> reply(json, command, sender))
                            : processNewState(remove(json, COMMAND_TECHNICAL), command, sender)),
        e -> {
          getLogger(LOGGER).severe(getStackTrace(e));
          sendToError(command);

          return null;
        });
  }

  private void handleIdle() {
    if (passivationTimeout != null
        && !between(touched, now()).minus(passivationTimeout).isNegative()) {
      passivate(sender());
    }
  }

  private void handleSnapshot(final SnapshotOffer offer) {
    state =
        Json.from((String) offer.snapshot())
            .filter(Json::isObject)
            .map(JsonValue::asJsonObject)
            .map(this::validateSnapshot)
            .orElseThrow(() -> new GeneralException("Corrupt snapshot"));
  }

  private boolean isNext(final JsonObject event) {
    return isNext(state, event);
  }

  private boolean matches(final JsonObject command) {
    return persistenceId().equalsIgnoreCase(command.getString(ID, ""))
        && type.equals(command.getString(TYPE, ""));
  }

  private void next() {
    context()
        .system()
        .scheduler()
        .scheduleOnce(
            scala.concurrent.duration.Duration.create(
                passivationTimeout.toMillis(), TimeUnit.MILLISECONDS),
            self(),
            IDLE,
            context().system().dispatcher(),
            null);
  }

  private void passivate(final ActorRef sender) {
    replyEmpty(sender);
    context().parent().tell(new ShardRegion.Passivate(PoisonPill.getInstance()), self());
  }

  public String persistenceId() {
    return self().path().name();
  }

  @Override
  public void preStart() {
    if (passivationTimeout != null) {
      next();
    }
  }

  private void processEvent(
      final JsonObject event, final JsonObject command, final ActorRef sender) {
    if (isCommand(command, GET)) {
      sender.tell(event.getJsonObject(AFTER), self());
    } else {
      if (isFirst(event) || changes(event)) {
        save(event, command, sender);
      } else {
        replyEmpty(sender);
      }
    }
  }

  private CompletionStage<Void> processNewState(
      final JsonObject newState, final JsonObject command, final ActorRef sender) {
    return runAsync(
        () ->
            processEvent(
                checkEvent(
                    state,
                    getEmbeddedIdPaths(state).collect(toSet()),
                    createEvent(state, newState, command)),
                command,
                sender));
  }

  private void reply(final JsonObject json, final JsonObject command, final ActorRef sender) {
    final Optional<String> user = getUser(json);

    log("Reply", json);

    if (!isCommand(command, GET)) {
      getLogger(LOGGER).log(INFO, "{0}", string(json));
    }

    if (!user.isPresent() || isCommand(command, GET) || isTest(command)) {
      sender.tell(json, self());
    } else {
      sendNotification(sns, snsTopic, json);
      replyEmpty(sender);
    }
  }

  private void replyEmpty(final ActorRef sender) {
    sender.tell(emptyObject(), self());
  }

  private void replyNotFound(final JsonObject command) {
    final Optional<String> user = getUser(command);

    if (user.isPresent() && !isCommand(command, GET)) {
      reply(setNotFound(command), command, sender());
    } else {
      replyEmpty(sender());
    }
  }

  private void save(final JsonObject event, final JsonObject command, final ActorRef sender) {
    log("Save", event);

    from(string(removeStates(event)))
        .accept(
            ev ->
                persist(
                    ev,
                    e -> {
                      tryToDoWithRethrow(
                          () -> createProducer(producerConfig),
                          producer ->
                              producer.send(
                                  record(setTest(event, command), aggregateTopic(event))));

                      Optional.ofNullable(event.getJsonObject(AFTER))
                          .ifPresent(
                              newState -> {
                                reply(newState, command, sender);
                                state = newState;
                              });

                      if (event.getInt(SEQ) % snapshotInterval == snapshotInterval - 1) {
                        saveSnapshot(string(state));
                      }
                    }));
  }

  private void sendToError(final JsonObject command) {
    tryToDoWithRethrow(
        () -> createProducer(producerConfig),
        producer -> producer.send(record(command, aggregateTopic(command) + "-error")));
  }

  private JsonObject setTechnical(final JsonObject newState, final JsonObject event) {
    final JsonObjectBuilder builder =
        removeTechnical(newState)
            .add(CORR, event.getString(CORR))
            .add(ID, event.getString(ID).toLowerCase())
            .add(TYPE, event.getString(TYPE))
            .add(SEQ, event.getInt(SEQ));

    return getValue(event, "/" + JWT)
        .filter(Json::isObject)
        .map(JsonValue::asJsonObject)
        .map(json -> builder.add(JWT, json))
        .orElseGet(() -> builder.remove(JWT))
        .build();
  }

  private void update(final String event) {
    state =
        Json.from(event)
            .filter(Json::isObject)
            .map(JsonValue::asJsonObject)
            .map(
                json -> pair(json, createPatch(validateEvent(json).getJsonArray(OPS)).apply(state)))
            .map(pair -> setTechnical(pair.second, pair.first))
            .orElseThrow(() -> new GeneralException("Corrupt event \"" + event + "\""));
  }

  private JsonObject validateEvent(final JsonObject event) {
    if (!persistenceId().equalsIgnoreCase(event.getString(ID)) && !isNext(event)) {
      throwCorruptEvent(persistenceId(), event);
    }

    return event;
  }

  private JsonObject validateSnapshot(final JsonObject snapshot) {
    if (!persistenceId().equalsIgnoreCase(snapshot.getString(ID))) {
      throw new GeneralException(
          "Corrupt snapshot " + string(snapshot) + " for ID " + persistenceId());
    }

    return snapshot;
  }
}
