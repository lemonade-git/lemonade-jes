package be.lemonade.jes.akka;

import akka.actor.ActorRef;

/**
 * An actor reference wrapper to enable @Provides injection of the aggregate root dispatcher.
 *
 * @author Werner Donn\u00e9
 */
public class JsonAggregateRootRef {
  private final ActorRef actorRef;

  JsonAggregateRootRef(final ActorRef actorRef) {
    this.actorRef = actorRef;
  }

  public ActorRef actorRef() {
    return actorRef;
  }
}
