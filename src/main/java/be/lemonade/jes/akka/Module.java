package be.lemonade.jes.akka;

import be.lemonade.jes.aws.EmailClient;
import be.lemonade.jes.aws.EmailClientProvider;
import be.lemonade.jes.aws.S3ClientProvider;
import be.lemonade.jes.aws.SNSClientProvider;
import be.lemonade.jes.mongo.ClientProvider;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.mongodb.reactivestreams.client.MongoClient;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.sns.SnsAsyncClient;

/**
 * The dependency injection module for the aggregate root dispatcher, the registration of the JSON
 * processors and AWS services.
 *
 * @author Werner Donn\u00e9
 */
public class Module extends AbstractModule {
  @Override
  protected void configure() {
    bind(JsonAggregateRootRef.class).toProvider(JsonAggregateRootProvider.class);
    bind(MongoClient.class).toProvider(ClientProvider.class);
    bind(SnsAsyncClient.class).toProvider(SNSClientProvider.class);
    bind(S3AsyncClient.class).toProvider(S3ClientProvider.class);
    bind(EmailClient.class).toProvider(EmailClientProvider.class);
  }

  @Provides
  @Singleton
  public JsonProcessorRegistration processorRegistration() {
    return new JsonProcessorRegistration();
  }
}
