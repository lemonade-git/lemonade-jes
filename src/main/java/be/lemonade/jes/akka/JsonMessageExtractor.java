package be.lemonade.jes.akka;

import static be.lemonade.jes.util.JsonFields.ID;

import akka.cluster.sharding.ShardRegion;
import javax.json.JsonObject;

/**
 * Uses the field "_id" as the entity ID.
 *
 * @author Werner Donn\u00e9
 */
public class JsonMessageExtractor extends ShardRegion.HashCodeMessageExtractor {
  public JsonMessageExtractor(final int maxNumberOfShards) {
    super(maxNumberOfShards);
  }

  public String entityId(final Object message) {
    return ((JsonObject) message).getString(ID, "");
  }
}
