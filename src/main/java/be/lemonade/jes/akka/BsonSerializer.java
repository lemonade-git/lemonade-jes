package be.lemonade.jes.akka;

import static be.lemonade.jes.util.Bson.fromBson;
import static be.lemonade.jes.util.Bson.fromBytes;
import static be.lemonade.jes.util.Bson.fromJson;
import static be.lemonade.jes.util.Bson.toBytes;

import akka.serialization.JSerializer;
import javax.json.JsonValue;

/**
 * Serializes only <code>javax.json.JsonValue</code>.
 *
 * @author Werner Donn\u00e9
 */
public class BsonSerializer extends JSerializer {
  @Override
  public Object fromBinaryJava(final byte[] bytes, final Class<?> c) {
    return fromBson(fromBytes(bytes));
  }

  @Override
  public int identifier() {
    return 1285209;
  }

  @Override
  public boolean includeManifest() {
    return false;
  }

  @Override
  public byte[] toBinary(final Object o) {
    return toBytes(fromJson((JsonValue) o));
  }
}
