package be.lemonade.jes.akka;

import static be.lemonade.jes.util.JsonFields.TYPE;
import static be.lemonade.jes.util.Logging.LOGGER;
import static be.lemonade.jes.util.Logging.log;
import static java.util.logging.Level.INFO;
import static java.util.logging.Logger.getLogger;
import static java.util.stream.Collectors.toMap;
import static net.pincette.util.Json.emptyObject;
import static net.pincette.util.Pair.pair;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.sharding.ClusterSharding;
import akka.cluster.sharding.ClusterShardingSettings;
import com.typesafe.config.Config;
import java.util.Map;
import java.util.stream.Collectors;
import javax.json.JsonObject;
import software.amazon.awssdk.services.sns.SnsAsyncClient;

/**
 * Creates cluster shards for the configured aggregate types and dispatches messages to the shards
 * based on the types. Reducers and validators can be registered.
 *
 * <p>The configuration should provide a Kafka configuration under <code>
 * be.lemonade.jes.kafka.config</code> and the number <code>be.lemonade.jes
 * .maxNumberOfShards</code>, which has 10 as the default value.
 *
 * @author Werner Donn\u00e9
 */
public class JsonAggregateRootDispatcher extends AbstractActor {
  private final Config config;
  private final JsonProcessorRegistration processors;
  private final SnsAsyncClient sns;
  private Map<String, ActorRef> regions;

  public JsonAggregateRootDispatcher(
      final JsonProcessorRegistration processors, final SnsAsyncClient sns, final Config config) {
    this.processors = processors;
    this.sns = sns;
    this.config = config;
  }

  public Receive createReceive() {
    return receiveBuilder().match(JsonObject.class, this::handleMessage).build();
  }

  private void handleMessage(final JsonObject message) {
    loadRegions();

    if (!message.containsKey(TYPE)) {
      log("Can't dispatch", message);
    } else {
      final ActorRef region = regions.get(message.getString(TYPE));

      if (region != null) {
        log("Dispatch", message);
        region.forward(message, context());
      } else {
        log("No shard region for type " + message.getString(TYPE, null), message);
        replyEmpty(sender());
      }
    }
  }

  private void loadRegions() {
    if (regions == null) {
      getLogger(LOGGER)
          .log(
              INFO,
              "Load shard regions for aggregate types {0}",
              new String[] {processors.types().collect(Collectors.joining(", "))});

      final ClusterShardingSettings settings = ClusterShardingSettings.create(context().system());
      final ClusterSharding sharding = ClusterSharding.get(context().system());

      regions =
          processors
              .types()
              .map(
                  type ->
                      pair(
                          type,
                          sharding.start(
                              type,
                              Props.create(
                                  JsonAggregateRoot.class,
                                  type,
                                  processors.getValidator(type),
                                  processors.getReducer(type),
                                  sns,
                                  config),
                              settings,
                              new JsonMessageExtractor(
                                  config.hasPath("be.lemonade.jes.maxNumberOfShards")
                                      ? config.getInt("be.lemonade.jes.maxNumberOfShards")
                                      : 10))))
              .collect(toMap(pair -> pair.first, pair -> pair.second));
    }
  }

  private void replyEmpty(final ActorRef sender) {
    sender.tell(emptyObject(), self());
  }
}
