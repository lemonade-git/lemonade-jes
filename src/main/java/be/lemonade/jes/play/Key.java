package be.lemonade.jes.play;

import static java.security.KeyFactory.getInstance;
import static java.util.Base64.getDecoder;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.joining;
import static net.pincette.util.Util.tryToGetRethrow;
import static net.pincette.util.Util.tryToGetWith;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Optional;
import net.pincette.function.SupplierWithException;

/**
 * Some utilities for using keys.
 *
 * @author Werner Donn\u00e9
 */
public class Key {
  private Key() {}

  public static Optional<String> getKey(final String key) {
    return getKey(() -> new StringReader(key));
  }

  /**
   * The <code>key</code> will be closed.
   *
   * @param key the input stream of which an ASCII reader is created.
   * @return The Base64 encoded key.
   */
  public static Optional<String> getKey(final InputStream key) {
    return getKey(() -> new InputStreamReader(key, "ASCII"));
  }

  /**
   * The reader will be closed.
   *
   * @param getReader the function to get the reader.
   * @return The Base64 encoded key.
   */
  public static Optional<String> getKey(final SupplierWithException<Reader> getReader) {
    return tryToGetWith(
            getReader,
            reader ->
                Optional.of(
                        new BufferedReader(reader)
                            .lines()
                            .map(String::trim)
                            .filter(line -> !line.startsWith("-----"))
                            .collect(joining()))
                    .filter(s -> s.length() > 0))
        .orElse(empty());
  }

  /**
   * Creates a private RSA key.
   *
   * @param key the Base64 encoded key in PKCS8 format.
   * @return The private key.
   */
  public static Optional<RSAPrivateKey> getRSAPrivateKey(final String key) {
    return tryToGetRethrow(
        () ->
            (RSAPrivateKey)
                getInstance("RSA")
                    .generatePrivate(new PKCS8EncodedKeySpec(getDecoder().decode(key))));
  }

  /**
   * Creates a public RSA key.
   *
   * @param key the Base64 encoded key in PEM format.
   * @return The public key.
   */
  public static Optional<RSAPublicKey> getRSAPublicKey(final String key) {
    return tryToGetRethrow(
        () ->
            (RSAPublicKey)
                getInstance("RSA")
                    .generatePublic(new X509EncodedKeySpec(getDecoder().decode(key))));
  }
}
