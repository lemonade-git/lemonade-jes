package be.lemonade.jes.play;

import static akka.pattern.Patterns.ask;
import static akka.util.ByteString.fromString;
import static be.lemonade.jes.play.Util.getBearerToken;
import static be.lemonade.jes.play.Util.getJwtKey;
import static be.lemonade.jes.play.Util.getPayload;
import static be.lemonade.jes.play.Util.getUser;
import static be.lemonade.jes.play.Util.writeJson;
import static be.lemonade.jes.util.Aggregate.isDeleted;
import static be.lemonade.jes.util.Aggregate.sendCommandInternal;
import static be.lemonade.jes.util.Bson.fromJson;
import static be.lemonade.jes.util.Command.createCommand;
import static be.lemonade.jes.util.Command.createCommandBuilder;
import static be.lemonade.jes.util.Command.isCommand;
import static be.lemonade.jes.util.Commands.DELETE;
import static be.lemonade.jes.util.Commands.GET;
import static be.lemonade.jes.util.Commands.PATCH;
import static be.lemonade.jes.util.Commands.PUT;
import static be.lemonade.jes.util.JsonFields.COMMAND;
import static be.lemonade.jes.util.JsonFields.CORR;
import static be.lemonade.jes.util.JsonFields.ID;
import static be.lemonade.jes.util.JsonFields.JWT;
import static be.lemonade.jes.util.JsonFields.LANGUAGES;
import static be.lemonade.jes.util.JsonFields.TYPE;
import static be.lemonade.jes.util.Logging.LOGGER;
import static be.lemonade.jes.util.Logging.log;
import static be.lemonade.jes.util.Util.NOT_DELETED;
import static be.lemonade.jes.util.Util.addNotDeleted;
import static be.lemonade.jes.util.Util.isManagedObject;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Optional.empty;
import static java.util.UUID.randomUUID;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.logging.Level.INFO;
import static java.util.logging.Logger.getLogger;
import static java.util.stream.Collectors.toList;
import static javax.json.Json.createArrayBuilder;
import static javax.json.Json.createObjectBuilder;
import static net.pincette.util.Builder.create;
import static net.pincette.util.Collections.indexedStream;
import static net.pincette.util.Collections.list;
import static net.pincette.util.Collections.map;
import static net.pincette.util.Collections.set;
import static net.pincette.util.Json.addIf;
import static net.pincette.util.Json.asNumber;
import static net.pincette.util.Json.emptyObject;
import static net.pincette.util.Json.from;
import static net.pincette.util.Json.hasErrors;
import static net.pincette.util.Json.string;
import static net.pincette.util.Pair.pair;
import static net.pincette.util.Util.getSegments;
import static net.pincette.util.Util.tryToGet;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.japi.pf.PFBuilder;
import akka.stream.Graph;
import akka.stream.SourceShape;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import be.lemonade.jes.akka.JsonAggregateRootRef;
import be.lemonade.jes.akka.JsonProcessorRegistration;
import be.lemonade.jes.kafka.JsonConsumer;
import be.lemonade.jes.kafka.JsonConsumerActor;
import be.lemonade.jes.util.Bson;
import be.lemonade.jes.util.JsonFields;
import com.mongodb.MongoException;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoCollection;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.typesafe.config.Config;
import java.security.Key;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import net.pincette.function.SideEffect;
import net.pincette.util.Json;
import net.pincette.util.Pair;
import net.pincette.util.Triple;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonInt32;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.json.JsonParseException;
import org.reactivestreams.Publisher;
import play.http.HttpEntity;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http.Request;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import play.mvc.Results;
import scala.compat.java8.FutureConverters;

/**
 * The REST API for JSON aggregate roots. The data is the API.
 *
 * @author Werner Donn\u00e9
 */
@Singleton
public class JsonAggregateController extends Controller {
  private static final String AGGREGATE = "aggregate";
  private static final String MATCH = "$match";
  private static final String QUERY = "query";
  private static final String PAGE = "page";
  private static final String PAGESIZE = "pagesize";
  private static final String PROJECT = "$project";
  private static final String PROJECTION = "projection";
  private static final String TEST = "test";
  private static final String UTF8 = "UTF-8";

  private final ActorSystem actorSystem;
  private final String commandTopic;
  private final Config config;
  private final ActorRef dispatcher;
  private final HttpExecutionContext httpContext;
  private final MongoDatabase mongoDatabase;
  private final Key jwtKey;

  @Inject
  public JsonAggregateController(
      final HttpExecutionContext httpContext,
      final JsonAggregateRootRef rootRef,
      final JsonProcessorRegistration processors,
      final ActorSystem actorSystem,
      final MongoClient mongoClient,
      final Config config) {
    this.httpContext = httpContext;
    dispatcher = rootRef.actorRef();
    this.actorSystem = actorSystem;
    this.config = config;
    jwtKey = getJwtKey(config);
    commandTopic = config.getString("be.lemonade.jes.kafka.command.topic");
    mongoDatabase =
        Optional.ofNullable(mongoClient)
            .map(c -> c.getDatabase(config.getString("be.lemonade.jes.mongodb.database")))
            .orElse(null);
    startCommandConsumer();
    processors.onInitialized(proc -> startConsumers(proc.getConsumers()));
  }

  private static BsonDocument addExtraCriterion(
      final BsonDocument query, final JsonObject extraCriterion) {
    return extraCriterion != null
        ? new BsonDocument("$and", new BsonArray(list(query, fromJson(extraCriterion))))
        : query;
  }

  private static void addExtraCriterion(
      final List<BsonDocument> stages, final JsonObject extraCriterion) {
    if (extraCriterion != null) {
      getStage(stages, MATCH)
          .ifPresent(
              pair ->
                  stages.set(
                      pair.second,
                      new BsonDocument(
                          MATCH,
                          addExtraCriterion(pair.first.getDocument(MATCH), extraCriterion))));
    }
  }

  private static Publisher<Document> aggregate(
      final MongoCollection<Document> collection,
      final Map<String, String[]> queryString,
      final JsonObject body,
      final JsonObject extraCriterion) {
    return collection.aggregate(
        create(() -> new ArrayList<>(getAggregateStages(queryString, body)))
            .update(list -> setMatch(list, queryString, body, extraCriterion))
            .updateIf(
                list -> !hasStage(list, PROJECT),
                list ->
                    getProjection(queryString, body)
                        .ifPresent(
                            p ->
                                list.add(
                                    getStage(list, MATCH).map(pair -> pair.second + 1).orElse(0),
                                    p)))
            .update(
                list ->
                    getSkip(queryString, body)
                        .ifPresent(
                            skip -> list.add(new BsonDocument("$skip", new BsonInt32(skip)))))
            .update(
                list ->
                    getInt(queryString, body, PAGESIZE)
                        .ifPresent(
                            size -> list.add(new BsonDocument("$limit", new BsonInt32(size)))))
            .build());
  }

  private static Publisher<Document> find(
      final MongoCollection<Document> collection,
      final Map<String, String[]> queryString,
      final JsonObject body,
      final JsonObject extraCriterion) {
    return collection
        .find(
            getQuery(queryString, body, extraCriterion)
                .orElse(addExtraCriterion(NOT_DELETED, extraCriterion)))
        .limit(getInt(queryString, body, PAGESIZE).orElse(MAX_VALUE))
        .skip(getSkip(queryString, body).orElse(0))
        .projection(getObject(queryString, body, PROJECTION).orElse(null));
  }

  private static List<BsonDocument> getAggregateStages(
      final Map<String, String[]> queryString, final JsonObject body) {
    return getArray(queryString, body, AGGREGATE)
        .map(BsonArray::getValues)
        .map(Collection::stream)
        .map(
            stream ->
                stream
                    .map(BsonValue::asDocument)
                    .map(
                        doc ->
                            doc.containsKey(MATCH)
                                ? new BsonDocument(MATCH, addNotDeleted(doc.getDocument(MATCH)))
                                : doc))
        .orElseGet(Stream::empty)
        .collect(toList());
  }

  private static Optional<BsonArray> getArray(
      final Map<String, String[]> queryString, final JsonObject body, final String name) {
    return Optional.ofNullable(
        getParameter(queryString, name)
            .map(BsonArray::parse)
            .orElseGet(
                () ->
                    Optional.ofNullable(body.getJsonArray(name)).map(Bson::fromJson).orElse(null)));
  }

  private static Optional<Integer> getInt(
      final Map<String, String[]> queryString, final JsonObject body, final String name) {
    return getValue(queryString, body, name, Integer::parseInt, v -> asNumber(v).intValue());
  }

  private static JsonArray getLanguages(final RequestHeader request) {
    return createArrayBuilder(
            request.acceptLanguages().stream()
                .map(lang -> lang.toLocale().toLanguageTag())
                .collect(toList()))
        .build();
  }

  private static Optional<BsonDocument> getObject(
      final Map<String, String[]> queryString, final JsonObject body, final String name) {
    return Optional.ofNullable(
        getParameter(queryString, name)
            .map(BsonDocument::parse)
            .orElseGet(
                () ->
                    Optional.ofNullable(body.getJsonObject(name))
                        .map(Bson::fromJson)
                        .orElse(null)));
  }

  private static Optional<String> getParameter(
      final Map<String, String[]> queryString, final String name) {
    return Optional.ofNullable(queryString.get(name))
        .filter(values -> values.length > 0)
        .map(values -> values[0]);
  }

  private static Optional<BsonDocument> getProjection(
      final Map<String, String[]> queryString, final JsonObject body) {
    return wrapInStage(() -> getObject(queryString, body, PROJECTION), PROJECT);
  }

  private static Optional<BsonDocument> getQuery(
      final Map<String, String[]> queryString,
      final JsonObject body,
      final JsonObject extraCriterion) {
    return getObject(queryString, body, QUERY)
        .map(be.lemonade.jes.util.Util::addNotDeleted)
        .map(query -> addExtraCriterion(query, extraCriterion));
  }

  private static Optional<Integer> getSkip(
      final Map<String, String[]> queryString, final JsonObject body) {
    return getInt(queryString, body, PAGESIZE)
        .flatMap(size -> getInt(queryString, body, PAGE).map(page -> pair(size, page)))
        .map(pair -> (pair.second - 1) * pair.first);
  }

  private static Optional<Pair<BsonDocument, Integer>> getStage(
      final List<BsonDocument> stages, final String name) {
    return indexedStream(stages).filter(pair -> pair.first.containsKey(name)).findFirst();
  }

  private static <T> Optional<T> getValue(
      final Map<String, String[]> queryString,
      final JsonObject body,
      final String name,
      final Function<String, T> fromString,
      final Function<JsonValue, T> fromValue) {
    return Optional.ofNullable(
        getParameter(queryString, name)
            .map(fromString)
            .orElseGet(
                () ->
                    net.pincette.util.Json.getValue(body, "/" + name).map(fromValue).orElse(null)));
  }

  private static boolean hasStage(final List<BsonDocument> stages, final String name) {
    return getStage(stages, name).isPresent();
  }

  private static JsonObjectBuilder initialBuilder(
      final JsonObject json, final String type, final String id, final JsonObject jwt) {
    return addIf(
        addIf(
            createObjectBuilder(json).add(ID, id.toLowerCase()).add(TYPE, type).remove(JWT),
            () -> jwt != null,
            b -> b.add(JWT, jwt)),
        () -> !json.containsKey(CORR),
        b -> b.add(CORR, randomUUID().toString()));
  }

  private static void logProduce(final JsonObject command, final Config config) {
    getLogger(LOGGER)
        .log(
            INFO,
            "Produce {0} on topic {1}",
            new Object[] {
              string(command), config.getString("be.lemonade.jes.kafka.command.topic")
            });
  }

  private static void setMatch(
      final List<BsonDocument> stages,
      final Map<String, String[]> queryString,
      final JsonObject body,
      final JsonObject extraCriterion) {
    if (hasStage(stages, MATCH)) {
      addExtraCriterion(stages, extraCriterion);
    } else {
      wrapInStage(() -> getQuery(queryString, body, extraCriterion), MATCH)
          .ifPresent(q -> stages.add(0, q));
    }
  }

  private static Optional<BsonDocument> wrapInStage(
      final Supplier<Optional<BsonDocument>> element, final String stage) {
    return element
        .get()
        .map(Bson::fromBson)
        .map(e -> createObjectBuilder().add(stage, e).build())
        .map(Bson::fromJson);
  }

  @BodyParser.Of(BodyParser.Bytes.class)
  public CompletionStage<Result> delete(final String type, final String id, final Request request) {
    return send(
        type,
        id,
        createObjectBuilder(createCommand(getType(type, request), id, DELETE))
            .add(LANGUAGES, getLanguages(request))
            .build(),
        b -> b,
        request);
  }

  private CompletionStage<Result> direct(final JsonObject command) {
    return FutureConverters.toJava(ask(dispatcher, command, 30000))
        .thenApply(reply -> (JsonObject) reply)
        .thenApply(reply -> directReply(reply, command));
  }

  private Result directReply(final JsonObject reply, final JsonObject command) {
    final IntSupplier result = () -> hasErrors(reply) ? 409 : 200;

    return !reply.isEmpty()
            && (!isManagedObject(reply, command.getString(TYPE), command.getString(ID))
                || (!isCommand(command, DELETE) && isDeleted(reply)))
        ? notFound()
        : writeJson(reply, result.getAsInt());
  }

  private CompletionStage<Boolean> dispatchCommand(final JsonObject command) {
    log("Receive", command);

    dispatcher.tell(command, ActorRef.noSender());

    return completedFuture(true);
  }

  public CompletionStage<Result> get(final String type, final String id, final Request request) {
    return getJwt(request)
        .map(
            jwt ->
                direct(
                    createCommandBuilder(getType(type, request), id, GET)
                        .add(JWT, jwt)
                        .add(LANGUAGES, getLanguages(request))
                        .build()))
        .orElseGet(() -> completedFuture(forbidden()));
  }

  private Optional<String> getApp(final Request request) {
    return getSegments(request.path(), "/").findFirst();
  }
  /**
   * Extracts the payload from the provided JSON Web Token.
   *
   * @return The payload.
   */
  protected Optional<JsonObject> getJwt(final Request request) {
    return getBearerToken(request).flatMap(token -> getPayload(token, jwtKey));
  }

  private String getType(final String type, final Request request) {
    return type.indexOf('-') != -1
        ? type
        : getApp(request).map(app -> app + "-" + type).orElse(type);
  }

  public CompletionStage<Result> health() {
    return completedFuture(ok());
  }

  private CompletionStage<Optional<JsonObject>> parse(final Request request) {
    return CompletableFuture.supplyAsync(
            () -> pair(request.body().asBytes().decodeString(UTF8), getLanguages(request)),
            httpContext.current())
        .thenApplyAsync(pair -> pair(from(pair.first), pair.second))
        .thenApply(
            pair ->
                pair.first
                    .filter(Json::isObject)
                    .map(JsonValue::asJsonObject)
                    .map(json -> createObjectBuilder(json).add(LANGUAGES, pair.second).build()));
  }

  @BodyParser.Of(BodyParser.Bytes.class)
  public CompletionStage<Result> patch(final String type, final String id, final Request request) {
    return send(type, id, null, b -> b.add(COMMAND, PATCH), request);
  }

  @BodyParser.Of(BodyParser.Bytes.class)
  public CompletionStage<Result> post(final String type, final String id, final Request request) {
    return send(type, id, null, b -> b, request);
  }

  @BodyParser.Of(BodyParser.Bytes.class)
  public CompletionStage<Result> put(final String type, final String id, final Request request) {
    return send(type, id, null, b -> b.add(COMMAND, PUT), request);
  }

  @BodyParser.Of(BodyParser.Bytes.class)
  public CompletionStage<Result> searchMongo(final String type, final Request request) {
    return searchMongo(type, null, request);
  }

  /**
   * Searches MongoDB.
   *
   * @param type the aggregate type.
   * @param extraCriterion an extra criterion with which the given query is always anded. It may be
   *     <code>null</code>.
   * @return The result stream.
   */
  protected CompletionStage<Result> searchMongo(
      final String type, final JsonObject extraCriterion, final Request request) {
    final String fullType = getType(type, request);
    final Map<String, String[]> queryString = request.queryString();

    return getJwt(request)
        .map(
            jwt ->
                (request.method().equals("POST")
                        ? parse(request)
                        : completedFuture(Optional.of(emptyObject())))
                    .thenApplyAsync(
                        body ->
                            body.map(
                                    b ->
                                        Optional.ofNullable(mongoDatabase)
                                            .map(
                                                d ->
                                                    searchMongo(
                                                        queryString, b, fullType, extraCriterion))
                                            .orElseGet(Results::notFound))
                                .orElseGet(Results::badRequest)))
        .orElseGet(() -> completedFuture(forbidden()));
  }

  private Result searchMongo(
      final Map<String, String[]> queryString,
      final JsonObject body,
      final String type,
      final JsonObject extraCriterion) {
    return tryToGet(
            () ->
                new Result(
                    200,
                    new HttpEntity.Streamed(
                        searchMongoSource(
                            mongoDatabase.getCollection(type), queryString, body, extraCriterion),
                        empty(),
                        Optional.of("application/json"))),
            e -> e instanceof JsonParseException ? badRequest() : internalServerError())
        .orElseGet(Results::internalServerError);
  }

  private Source<ByteString, NotUsed> searchMongoSource(
      final MongoCollection<Document> collection,
      final Map<String, String[]> queryString,
      final JsonObject body,
      final JsonObject extraCriterion) {
    return Source.fromPublisher(
            queryString.containsKey(AGGREGATE) || body.containsKey(AGGREGATE)
                ? aggregate(collection, queryString, body, extraCriterion)
                : find(collection, queryString, body, extraCriterion))
        .map(Document::toJson)
        .map(ByteString::fromString)
        .log(LOGGER)
        .recoverWithRetries(
            1,
            new PFBuilder<Throwable, Graph<SourceShape<ByteString>, NotUsed>>()
                .match(MongoException.class, ex -> Source.from(list(fromString(ex.getMessage()))))
                .build())
        .intersperse(fromString("["), fromString(","), fromString("]"));
  }

  private CompletionStage<Result> send(
      final String type,
      final String id,
      final JsonObject command,
      final UnaryOperator<JsonObjectBuilder> builder,
      final Request request) {
    final String fullType = getType(type, request);
    final boolean test = testMode(request);

    return getJwt(request)
        .map(
            jwt ->
                Optional.ofNullable(command)
                    .map(
                        c ->
                            (CompletionStage<Optional<JsonObject>>) completedFuture(Optional.of(c)))
                    .orElseGet(() -> parse(request))
                    .thenComposeAsync(
                        json ->
                            json.filter(Util::noDots)
                                .map(
                                    j ->
                                        writeCommand(
                                            builder
                                                .apply(initialBuilder(j, fullType, id, jwt))
                                                .build(),
                                            test))
                                .orElse(completedFuture(badRequest()))))
        .orElseGet(() -> completedFuture(forbidden()));
  }

  public CompletionStage<Result> serverSentEvents(final Request request) {
    return getBearerToken(request)
        .flatMap(j -> getUser(j, jwtKey))
        .map(
            user ->
                completedFuture(
                    redirect(config.getString("be.lemonade.jes.fanoutUrl") + "?u=" + user)))
        .orElseGet(() -> completedFuture(forbidden()));
  }

  private void startCommandConsumer() {
    startConsumer(
        commandTopic,
        config.getString("be.lemonade.jes.kafka.command.groupId"),
        (key, command) -> dispatchCommand(command));
  }

  private ActorRef startConsumer(
      final String topic, final String groupId, final JsonConsumer consumer) {
    getLogger(LOGGER).log(INFO, "Start consumer {0} for topic {1}", new String[] {groupId, topic});

    return actorSystem.actorOf(
        Props.create(
                JsonConsumerActor.class,
                set(topic),
                new JsonConsumerActor.Config(
                    consumer,
                    config.getConfig("be.lemonade.jes"),
                    map(pair("group.id", groupId)),
                    false))
            .withDispatcher("kafka-dispatcher"));
  }

  private void startConsumers(final Stream<Triple<String, String, JsonConsumer>> consumers) {
    consumers.forEach(triple -> startConsumer(triple.first, triple.second, triple.third));
  }

  private boolean testMode(final Request request) {
    return Optional.ofNullable(request.getQueryString(TEST)).map(Boolean::new).orElse(false);
  }

  private CompletionStage<Result> writeCommand(final JsonObject command, final boolean testMode) {
    final Function<Boolean, Integer> statusCode =
        result -> result ? ACCEPTED : INTERNAL_SERVER_ERROR;

    log("Send", command);

    return testMode
        ? direct(createObjectBuilder(command).add(JsonFields.TEST, true).build())
        : SideEffect.<CompletionStage<Result>>run(() -> logProduce(command, config))
            .andThenGet(
                () ->
                    sendCommandInternal(command, config)
                        .thenApply(r -> (Result) status(statusCode.apply(r))));
  }
}
