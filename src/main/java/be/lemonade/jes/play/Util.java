package be.lemonade.jes.play;

import static akka.util.ByteString.fromString;
import static io.jsonwebtoken.Jwts.parser;
import static java.net.URLDecoder.decode;
import static java.util.stream.Stream.concat;
import static java.util.stream.Stream.of;
import static net.pincette.util.Json.from;
import static net.pincette.util.Json.nestedObjects;
import static net.pincette.util.Json.string;
import static net.pincette.util.Or.tryWith;
import static net.pincette.util.Pair.pair;
import static net.pincette.util.Util.tryToGet;
import static net.pincette.util.Util.tryToGetRethrow;

import com.typesafe.config.Config;
import io.jsonwebtoken.Claims;
import java.security.interfaces.RSAPublicKey;
import java.util.Optional;
import javax.json.JsonObject;
import javax.json.JsonStructure;
import javax.json.JsonValue;
import net.pincette.util.Json;
import net.pincette.util.Pair;
import net.pincette.util.Util.GeneralException;
import play.http.HttpEntity;
import play.mvc.Http.Cookie;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.StatusHeader;

/**
 * Some utilities for Play controllers.
 *
 * @author Werner Donn\u00e9
 */
public class Util {
  private static final String ACCESS_TOKEN = "access_token";

  private Util() {}

  /**
   * Extracts the bearer token from an HTTP request.
   *
   * @param request the HTTP request.
   * @return The bearer token.
   */
  public static Optional<String> getBearerToken(final RequestHeader request) {
    return tryWith(() -> getBearerTokenFromAuthorization(request))
        .or(() -> getBearerTokenFromQueryString(request))
        .or(() -> getBearerTokenFromCookie(request))
        .get()
        .flatMap(t -> tryToGet(() -> decode(t, "UTF-8")));
  }

  private static String getBearerTokenFromCookie(final RequestHeader request) {
    return Optional.ofNullable(request.cookie(ACCESS_TOKEN)).map(Cookie::value).orElse(null);
  }

  private static String getBearerTokenFromAuthorization(final RequestHeader request) {
    return request
        .header("Authorization")
        .map(header -> header.split(" "))
        .filter(s -> s.length == 2)
        .filter(s -> s[0].equalsIgnoreCase("Bearer"))
        .map(s -> s[1])
        .orElse(null);
  }

  private static String getBearerTokenFromQueryString(final RequestHeader request) {
    return Optional.ofNullable(request.getQueryString(ACCESS_TOKEN)).orElse(null);
  }

  /**
   * The function assumes the last segment of <code>href</code> is the ID and the segment before
   * that the aggregate type.
   *
   * @param href the given URL.
   * @return A pair where the first entry is the aggregate type and the second the ID.
   */
  public static Optional<Pair<String, String>> getTypeAndId(final String href) {
    return Optional.of(href.split("/"))
        .filter(segments -> segments.length > 1)
        .map(segments -> pair(segments[segments.length - 2], segments[segments.length - 1]));
  }

  /**
   * Creates a JWT verifier with the public key found in the configuration entry "be.lemonade.jes
   * .jwtRSAPublicKey".
   *
   * @param config the given configuration.
   * @return The JWT verifier.
   */
  public static RSAPublicKey getJwtKey(final Config config) {
    return Optional.ofNullable(config.getString("be.lemonade.jes.jwtRSAPublicKey"))
        .flatMap(Key::getKey)
        .flatMap(Key::getRSAPublicKey)
        .orElseThrow(() -> new GeneralException("Can't get JWT public key"));
  }

  public static Optional<JsonObject> getPayload(
      final String jwt, final java.security.Key publicKey) {
    return tryToGetRethrow(
        () -> from((Claims) parser().setSigningKey(publicKey).parse(jwt).getBody()));
  }

  /**
   * Extracts the username from the JWT payload. This is the value of the <code>sub</code> field.
   *
   * @param jwt the payload of the JWT.
   * @return The username.
   */
  public static Optional<String> getUser(final JsonObject jwt) {
    return Optional.ofNullable(jwt.getString("sub", null));
  }

  @SuppressWarnings("squid:S1612") // The suggested change puts everything before getSubject
  // outside of the try.
  public static Optional<String> getUser(final String jwt, final java.security.Key publicKey) {
    return tryToGet(
        () -> ((Claims) parser().setSigningKey(publicKey).parse(jwt).getBody()).getSubject());
  }

  public static boolean noDots(final JsonObject json) {
    return concat(of(json), nestedObjects(json))
        .flatMap(j -> j.keySet().stream())
        .noneMatch(key -> key.indexOf('.') != -1);
  }

  /**
   * Writes the JSON document to the response body with status code 200.
   *
   * @param json the JSON document.
   * @return The Play result object.
   */
  public static Result writeJson(final JsonStructure json) {
    return writeJson(json, 200);
  }

  /**
   * Writes the JSON document to the response body with the given status code.
   *
   * @param json the JSON document.
   * @param status the given status code.
   * @return The Play result object.
   */
  public static Result writeJson(final JsonStructure json, final int status) {
    final StatusHeader result = Results.status(status);

    return Optional.of(json)
        .filter(Json::isObject)
        .map(JsonValue::asJsonObject)
        .filter(JsonObject::isEmpty)
        .map(object -> (Result) result)
        .orElseGet(
            () ->
                result
                    .sendEntity(
                        new HttpEntity.Strict(
                            fromString(string(json, true)), Optional.of("application/json")))
                    .withHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store"));
  }
}
