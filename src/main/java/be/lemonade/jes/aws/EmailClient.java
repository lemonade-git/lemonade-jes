package be.lemonade.jes.aws;

import be.lemonade.aws.Email;
import be.lemonade.aws.Email.Message;
import java.util.concurrent.CompletionStage;
import software.amazon.awssdk.services.sns.SnsAsyncClient;

public class EmailClient {
  private final SnsAsyncClient sns;
  private final String topic;

  public EmailClient(final SnsAsyncClient sns, final String topic) {
    this.sns = sns;
    this.topic = topic;
  }

  public CompletionStage<String> send(final Message message) {
    return Email.send(message, sns, topic);
  }

  public CompletionStage<String> sendHtml(
      final String from, final String[] to, final String subject, final String body) {
    return Email.sendHtml(from, to, subject, body, sns, topic);
  }

  public CompletionStage<String> sendText(
      final String from, final String[] to, final String subject, final String body) {
    return Email.sendText(from, to, subject, body, sns, topic);
  }
}
