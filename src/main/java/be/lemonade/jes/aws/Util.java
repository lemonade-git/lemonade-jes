package be.lemonade.jes.aws;

import static be.lemonade.jes.util.JsonFields.JWT;
import static be.lemonade.jes.util.Logging.LOGGER;
import static java.util.logging.Level.INFO;
import static java.util.logging.Logger.getLogger;
import static javax.json.Json.createObjectBuilder;
import static net.pincette.util.Collections.map;
import static net.pincette.util.Json.string;
import static net.pincette.util.Pair.pair;

import java.util.Optional;
import java.util.concurrent.CompletionStage;
import javax.json.JsonObject;
import net.pincette.function.SideEffect;
import software.amazon.awssdk.services.sns.SnsAsyncClient;
import software.amazon.awssdk.services.sns.model.MessageAttributeValue;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.PublishResponse;

public class Util {
  private Util() {}

  private static PublishRequest createSNSMessage(final String topic, final JsonObject json) {
    final PublishRequest.Builder builder =
        PublishRequest.builder().topicArn(topic).message(string(removeJwt(json)));

    Optional.ofNullable(json.getJsonObject(JWT))
        .flatMap(be.lemonade.jes.play.Util::getUser)
        .ifPresent(
            user ->
                builder.messageAttributes(
                    map(
                        pair(
                            "username",
                            MessageAttributeValue.builder()
                                .dataType("String")
                                .stringValue(user)
                                .build()))));

    return builder.build();
  }

  private static JsonObject removeJwt(final JsonObject json) {
    return createObjectBuilder(json).remove(JWT).build();
  }

  public static CompletionStage<String> sendNotification(
      final SnsAsyncClient sns, final String topic, final JsonObject json) {
    return sns.publish(createSNSMessage(topic, json))
        .thenApply(PublishResponse::messageId)
        .thenApply(
            id ->
                SideEffect.<String>run(
                        () -> getLogger(LOGGER).log(INFO, "SNS reply: {0}", new String[] {id}))
                    .andThenGet(() -> id));
  }
}
