package be.lemonade.jes.aws;

import static be.lemonade.aws.SNS.client;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.typesafe.config.Config;
import software.amazon.awssdk.services.sns.SnsAsyncClient;

public class SNSClientProvider implements Provider<SnsAsyncClient> {
  private final SnsAsyncClient amazonSNS;

  @Inject
  public SNSClientProvider(final Config config) {
    amazonSNS = client(config.getConfig("be.lemonade.jes.sns"));
  }

  public SnsAsyncClient get() {
    return amazonSNS;
  }
}
