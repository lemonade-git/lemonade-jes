package be.lemonade.jes.aws;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.typesafe.config.Config;
import software.amazon.awssdk.services.sns.SnsAsyncClient;

public class EmailClientProvider implements Provider<EmailClient> {
  private final EmailClient client;

  @Inject
  public EmailClientProvider(final SnsAsyncClient sns, final Config config) {
    client = new EmailClient(sns, config.getString("be.lemonade.jes.sns.emailTopic"));
  }

  public EmailClient get() {
    return client;
  }
}
