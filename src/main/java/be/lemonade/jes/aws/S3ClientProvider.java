package be.lemonade.jes.aws;

import static be.lemonade.aws.S3.client;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.typesafe.config.Config;
import software.amazon.awssdk.services.s3.S3AsyncClient;

public class S3ClientProvider implements Provider<S3AsyncClient> {
  private final S3AsyncClient amazonS3;

  @Inject
  public S3ClientProvider(final Config config) {
    amazonS3 = client(config.getConfig("be.lemonade.jes.s3"));
  }

  public S3AsyncClient get() {
    return amazonS3;
  }
}
