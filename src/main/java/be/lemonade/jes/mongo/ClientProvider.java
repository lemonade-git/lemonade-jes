package be.lemonade.jes.mongo;

import static com.mongodb.reactivestreams.client.MongoClients.create;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.mongodb.reactivestreams.client.MongoClient;
import com.typesafe.config.Config;

/**
 * Provides the MongoDB client.
 *
 * @author Werner Donn\u00e9
 */
@Singleton
public class ClientProvider implements Provider<MongoClient> {
  private final MongoClient client;

  @Inject
  public ClientProvider(final Config config) {
    client =
        config.hasPath("be.lemonade.jes.mongodb.uri")
            ? create(config.getString("be.lemonade.jes.mongodb.uri"))
            : null;
  }

  public MongoClient get() {
    return client;
  }
}
