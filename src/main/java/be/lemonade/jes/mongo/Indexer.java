package be.lemonade.jes.mongo;

import static be.lemonade.jes.aws.Util.sendNotification;
import static be.lemonade.jes.play.Util.noDots;
import static be.lemonade.jes.util.Aggregate.isDeleted;
import static be.lemonade.jes.util.JsonFields.AFTER;
import static be.lemonade.jes.util.JsonFields.ID;
import static be.lemonade.jes.util.JsonFields.JWT;
import static be.lemonade.jes.util.JsonFields.TEST;
import static be.lemonade.jes.util.JsonFields.TYPE;
import static be.lemonade.jes.util.Logging.LOGGER;
import static be.lemonade.jes.util.Logging.log;
import static com.mongodb.client.model.Filters.eq;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.logging.Logger.getGlobal;
import static java.util.logging.Logger.getLogger;
import static javax.json.Json.createObjectBuilder;
import static net.pincette.util.Json.string;
import static org.bson.Document.parse;

import be.lemonade.jes.kafka.JsonConsumer;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.typesafe.config.Config;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.logging.Level;
import javax.json.JsonObject;
import net.pincette.rs.LambdaSubscriber;
import software.amazon.awssdk.services.sns.SnsAsyncClient;

/**
 * This consumer listens to changes in an aggregate and updates a configured MongoDB collection with
 * the aggregates.
 *
 * @author Werner Donn\u00e9
 */
public class Indexer implements JsonConsumer {
  private final MongoDatabase database;
  private final SnsAsyncClient sns;
  private final String snsTopic;
  private final Function<JsonObject, CompletionStage<JsonObject>> transformer;

  public Indexer(final MongoClient client, final SnsAsyncClient sns, final Config config) {
    this(client, sns, config, null);
  }

  public Indexer(
      final MongoClient client,
      final SnsAsyncClient sns,
      final Config config,
      final Function<JsonObject, CompletionStage<JsonObject>> transformer) {
    getLogger(LOGGER).info("Initialising MongoDB indexer");
    this.sns = sns;
    this.transformer = transformer;
    database = client.getDatabase(config.getString("be.lemonade.jes.mongodb.database"));
    snsTopic = config.getString("be.lemonade.jes.sns.sseTopic");
    getLogger(LOGGER).info("MongoDB indexer initialised");
  }

  private static <T> LambdaSubscriber<T> completer(final CompletableFuture<Boolean> future) {
    return new LambdaSubscriber<>(
        result -> future.complete(true),
        () -> {},
        e -> {
          getGlobal().log(Level.SEVERE, e.getMessage(), e);
          future.complete(false);
        });
  }

  public CompletionStage<Boolean> apply(final String key, final JsonObject message) {
    return Optional.of(message.getJsonObject(AFTER))
        .filter(r -> !isDeleted(r))
        .map(
            r ->
                (transformer != null ? transformer.apply(r) : completedFuture(r))
                    .thenComposeAsync(this::update))
        .orElseGet(() -> delete(message));
  }

  private CompletionStage<Boolean> delete(final JsonObject message) {
    final CompletableFuture<Boolean> future = new CompletableFuture<>();

    log("Delete MongoDB index", message);

    database
        .getCollection(message.getString(TYPE))
        .deleteOne(eq(ID, message.getString(ID)))
        .subscribe(completer(future));

    return future.thenComposeAsync(result -> notification(message).thenApply(id -> true));
  }

  private CompletionStage<String> notification(final JsonObject json) {
    return Optional.of(json.getBoolean(TEST, false))
        .filter(test -> !test)
        .map(
            test ->
                sendNotification(
                    sns,
                    snsTopic,
                    createObjectBuilder()
                        .add(TYPE, json.getString(TYPE))
                        .add(JWT, json.getJsonObject(JWT))
                        .build()))
        .orElseGet(() -> completedFuture(null));
  }

  private CompletionStage<Boolean> update(final JsonObject aggregate) {
    if (!noDots(aggregate)) {
      log("ERROR: no dots allowed in keys", aggregate);

      return completedFuture(true);
    }

    final CompletableFuture<Boolean> future = new CompletableFuture<>();

    log("Update MongoDB index", aggregate);

    database
        .getCollection(aggregate.getString(TYPE))
        .replaceOne(
            eq(ID, aggregate.getString(ID)),
            parse(string(aggregate)),
            new ReplaceOptions().upsert(true))
        .subscribe(completer(future));

    return future.thenComposeAsync(result -> notification(aggregate).thenApply(id -> true));
  }
}
